package net.esyo.tileengine.rtype;

import static javafx.scene.input.KeyCode.SPACE;
import javafx.scene.input.KeyEvent;
import net.esyo.tileengine.engine.ObjectSet;
import net.esyo.tileengine.engine.core.Collidable;
import net.esyo.tileengine.engine.core.KeyFrames;
import net.esyo.tileengine.engine.core.OnKey;
import net.esyo.tileengine.engine.core.Rect2;
import net.esyo.tileengine.engine.core.Vect2;
import static net.esyo.tileengine.engine.core.utils.AngleUtils.A0;
import static net.esyo.tileengine.engine.core.utils.AngleUtils.A45;

/**
 *
 * @author rpetit
 */
public class Weapon extends Craft implements OnKey {

    private static final ObjectSet<Class<?>> UNCOLLIDE = ObjectSet.of(Bullet.class, SpaceCraft.class, Weapon.class, Eye.class);

    private final Craft owner;

    private double ACC = 0.1;
    private double SPEED = 0.5;
    private int FIRE_RATE = 100;
    private int BOUNCING_FIRE_RATE = 200;
    private int ARC_FIRE_RATE = 200;
    private boolean firing = false;
    private long lastFire = 0;
    private long lastBouncingFire = 0;
    private long lastArcFire = 0;
    private boolean attached = true;
    private boolean attaching;
    private boolean detaching;
    private int level = 1;

    public Weapon(final Producer producer, final SpaceCraft owner) {
        super("src/main/resources/rtype/Assets/Game/Starship/Starship2.png", Vect2.of(32, 28), producer);
        this.owner = owner;
        owner.weapon = this;
        firing = owner.firing;
        super.defaultTileCode(0);
        super.addKeyFrames("NORMAL", new KeyFrames(3, 4, 5, 6)).duration(100).play(true);
    }

    @Override
    public Rect2 getBounds() {
        return Rect2.of(this.position(), 32, 28);
    }
    
    private ArcRayParticle last;

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        super.update(screenSize, elapsedTime);
        if (isAttached()) {
            position(Vect2.of(this.owner.position().x().add(33), this.owner.position().y().sub(6)));
        } else {
            if (!attaching && !detaching) {
                acceleration(Vect2.of(0.1, 0.1));
            }
            //stick to owner with inertia
            if (position().y().lowerThan(owner.position().y().sub(6))) {
                velocity(Vect2.of(velocity().x(), 1));
            }
            if (position().y().greaterThan(owner.position().y().sub(6))) {
                velocity(Vect2.of(velocity().x(), -1));
            }
            if (!attaching && !detaching) {
                if (position().x().lowerThan(owner.position().x().add(401))) {
                    velocity(Vect2.of(1, velocity().y()));
                }
                if (position().x().greaterThan(owner.position().x().add(399))) {
                    velocity(Vect2.of(-1, velocity().y()));
                }
            }

        }
        if (detaching) {
            if (velocity().x().lowerThan(1)) {
                velocity().x(1);
            }
            if (position().x().greaterOrEquals(owner.position().x().add(400)) || position().x().greaterOrEquals(screenSize.x().add(33))) {// && position.y() == owner.position().y()) {
                detaching = false;
                acceleration(Vect2.zero());
                //velocity(Vect2.zero());
            }
        }
        if (attaching) {
            if (position().x().lowerOrEquals(this.owner.position().x().add(33))) {// && position.y() == owner.position().y()) {
                attaching = false;
                attached = true;
                acceleration(Vect2.zero());
                velocity(Vect2.zero());
            }
        }
        //Re-attach
        if (position().x().lowerOrEquals(this.owner.position().x().add(33))) {
            attaching = false;
            attached = true;
            acceleration(Vect2.zero());
            velocity(Vect2.zero());
        }

        if (position().x().greaterThan(screenSize.x().sub(33))) {
            position().x(screenSize.x().sub(33));
        }

        //Blue
        /*
        if (level == 1 && attached && this.firing && System.currentTimeMillis() - this.lastBouncingFire > BOUNCING_FIRE_RATE) {
            final OrthogonalBouncingRay bouncingRayUp = new OrthogonalBouncingRay(screenSize, this, A45)
                    .velocity(Vect2.of(7));
            this.producer.produce(bouncingRayUp.position(this.position().copy().add(Vect2.of(32, 15))));

            final OrthogonalBouncingRay bouncingRay = new OrthogonalBouncingRay(screenSize, this, A0)
                    .velocity(Vect2.of(7));
            this.producer.produce(bouncingRay.position(this.position().copy().add(Vect2.of(32, 15))));

            final OrthogonalBouncingRay bouncingRayDown = new OrthogonalBouncingRay(screenSize, this, -A45)
                    .velocity(Vect2.of(7));
            this.producer.produce(bouncingRayDown.position(this.position().copy().add(Vect2.of(32, 15))));
            this.lastBouncingFire = System.currentTimeMillis();
        }
        */

        //Yellow ?
        //Red
        if (level == 1 && attached && this.firing && System.currentTimeMillis() - this.lastBouncingFire > ARC_FIRE_RATE) {
            final ArcRayParticle arc = new ArcRayParticle(screenSize,this.position().copy().add(Vect2.of(32, 15)), this).velocity(Vect2.of(1,0));
            if(last!=null){
                arc.last(last);
            }
            last = arc;
            this.producer.produce(arc);

            this.lastArcFire = System.currentTimeMillis();
        }
        
        if (this.firing && System.currentTimeMillis() - this.lastFire > FIRE_RATE) {
            if (level == 0 && !isAttached()) {
                final Bullet bullet = new Bullet(screenSize, this)
                        .velocity(Vect2.of(SPEED * 4, SPEED * 4))
                        .acceleration(Vect2.of(ACC * 1.5, ACC * 1.5));
                this.producer.produce(bullet.position(this.position().copy().add(Vect2.of(32, 15))));
            }
            if (level == 1 && !isAttached()) {
                final Bullet bulletUp = new Bullet(screenSize, this)
                        .angle(Math.PI / 5)
                        .velocity(Vect2.of(SPEED * 4, SPEED * 4))
                        .acceleration(Vect2.of(ACC * 1.5, ACC * 1.5));
                this.producer.produce(bulletUp.position(this.position().copy().add(Vect2.of(32, 28))));
                final Bullet bulletDown = new Bullet(screenSize, this)
                        .angle(-Math.PI / 5)
                        .velocity(Vect2.of(SPEED * 4, SPEED * 4))
                        .acceleration(Vect2.of(ACC * 1.5, ACC * 1.5));
                this.producer.produce(bulletDown.position(this.position().copy().add(Vect2.of(32, 0))));
            }

            if (level == 2 && !isAttached()) {
                final Bullet bulletUp = new Bullet(screenSize, this)
                        .angle(Math.PI / 2)
                        .velocity(Vect2.of(SPEED * 4, SPEED * 4))
                        .acceleration(Vect2.of(ACC * 1.5, ACC * 1.5));
                this.producer.produce(bulletUp.position(this.position().copy().add(Vect2.of(15, 28))));
                final Bullet bulletDown = new Bullet(screenSize, this)
                        .angle(-Math.PI / 2)
                        .velocity(Vect2.of(SPEED * 4, SPEED * 4))
                        .acceleration(Vect2.of(ACC * 1.5, ACC * 1.5));
                this.producer.produce(bulletDown.position(this.position().copy().add(Vect2.of(15, 0))));
            }
            this.lastFire = System.currentTimeMillis();
        }
    }

    @Override
    public void explode() {
        //can't explode
    }

    @Override
    public void onExplode() {
        //can't explode
    }

    @Override
    public boolean intersect(final Collidable other) {
        if (UNCOLLIDE.contains(other.getClass())) {
            return false;
        }
        return super.intersect(other) && getBounds().intersect(other.getBounds());
    }

    @Override
    public boolean isExploded() {
        //can't explode
        return false;
    }

    @Override
    public boolean canExplode() {
        //can't explode
        return false;
    }

    @Override
    public void onKeyUp(final KeyEvent k) {
        if (k.getCode() == SPACE) {
            stopFire();
        }
    }

    @Override
    public void onKeyPress(final KeyEvent k) {
        if (k.getCode() == SPACE) {
            startFire();
        }
    }

    public void startFire() {
        if (this.owner.isAlive()) {
            this.firing = true;
        }
    }

    public void stopFire() {
        this.firing = false;
    }

    public void detach() {
        this.attached = false;
        this.detaching = true;
        this.attaching = false;
        velocity(Vect2.of(15, 0));
        acceleration(Vect2.of(-0.3, 0));
    }

    public void attach() {
        this.attaching = true;
        this.detaching = false;
        velocity(Vect2.of(-1, 0));
        acceleration(Vect2.of(-0.3, 0));
    }

    public boolean isAttached() {
        return this.attached;
    }

    @Override
    public int power() {
        return 3000;
    }

    @Override
    public Weapon hit(final Collidable c) {
        //do nothing
        return this;
    }

    @Override
    public void onHit() {
        //nothing
    }
}
