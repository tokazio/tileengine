package net.esyo.tileengine.rtype;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Renderable;
import net.esyo.tileengine.engine.core.Updatable;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author rpetit
 */
public class StarEmitter implements Renderable, Updatable {

    private final List<StarParticle> stars = new CopyOnWriteArrayList<>();

    public StarEmitter(final Vect2 screenSize, final int number) {
        for (int i = 0; i < number; i++) {
            stars.add(new StarParticle(screenSize, true));
        }
    }

    @Override
    public void render(final Graphics gc) {
        stars.forEach(s -> s.render(gc));
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        for (StarParticle star : stars) {
            star.update(screenSize, elapsedTime);
            if (!star.isVisible()) {
                stars.remove(star);
                stars.add(new StarParticle(screenSize, false));
            }
        }
    }
}
