package net.esyo.tileengine.rtype;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Positionable;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.effects.*;

/**
 *
 * @author rpetit
 */
public class ExplodeParticleEmitter implements LivingParticleEmitter<ExplodeParticle> {

    private final List<ExplodeParticle> particles = new CopyOnWriteArrayList<>();
    private final int number;

    private Vect2 position;
    private boolean exploded;

    public ExplodeParticleEmitter(int number) {
        this.number = number;
    }

    public void explode() {
        this.exploded = true;
        for (int i = 0; i < this.number; i++) {
            this.particles.add(createParticle());
        }
    }

    @Override
    public <T extends Positionable> T position(Vect2 point) {
        this.position = point.copy();
        return (T) this;
    }

    @Override
    public void render(final Graphics gc) {
        this.particles.forEach(particle -> particle.render(gc));
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        for (ExplodeParticle particle : this.particles) {
            if (particle.isAlive()) {
                particle.update(screenSize, elapsedTime);
            } else {
                this.particles.remove(particle);
            }
        }
    }

    @Override
    public ExplodeParticle resetParticle(ExplodeParticle p) {
        return p;
    }

    @Override
    public ExplodeParticle createParticle() {
        //circular random------
        final double a = Math.random() * 2 * Math.PI;
        final double w = Math.random() * 10 - 5;
        final double x = Math.cos(a) * w;
        final double y = Math.sin(a) * w;
        //---------------------
        final ExplodeParticle p = new ExplodeParticle()
                .size(Math.random() * 5)
                .timeToLive(1000)
                .velocity(Vect2.of(x, y))
                .acceleration(Vect2.of(x / 100, y / 100));
        return p.position(this.position.copy());
    }

    @Override
    public Vect2 position() {
        return this.position;
    }

    public boolean finished() {
        return this.exploded && this.particles.isEmpty();
    }

}
