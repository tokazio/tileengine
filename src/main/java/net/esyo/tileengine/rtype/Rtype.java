package net.esyo.tileengine.rtype;

import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.control.TextInputDialog;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javax.xml.bind.JAXBException;
import net.esyo.tileengine.engine.core.Engine;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.TileSetException;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.map.tmx.MapException;

/**
 *
 * @author rpetit
 */
public class Rtype extends Application {

    private final static Vect2 SCREEN_SIZE = Vect2.of(800, 600);
    private final static SpaceWorld SPACE_WORLD = new SpaceWorld(SCREEN_SIZE);

    private static Engine engine;

    @Override
    public void start(Stage theStage) throws FileNotFoundException, IOException, TileSetException, JAXBException, MapException {

        //2D engine to render the tiles/sprites
        engine = new Engine(theStage, SCREEN_SIZE) {

            @Override
            public void initialize(Graphics gc) {
                this.addKeyable(SPACE_WORLD);
            }

            @Override
            public void update(Graphics gc, long elapsedTime) {
                gc.setFill(0, 0.3);
                gc.fillRect(gc.screenSize());
                if (!SPACE_WORLD.isFinished()) {
                    //updating
                    SPACE_WORLD.update(gc.screenSize(), elapsedTime);
                    //rendering
                    SPACE_WORLD.render(gc);
                } else {
                    gc.setStroke(Color.WHITE);
                    if (SPACE_WORLD.win()) {
                        gc.strokeText("Gagné! avec " + SPACE_WORLD.points + " points", gc.screenSize().x().half().asDouble(), gc.screenSize().y().half().asDouble());
                    } else {
                        gc.strokeText("Perdu! avec " + SPACE_WORLD.points + " points", gc.screenSize().x().half().asDouble(), gc.screenSize().y().half().asDouble());
                    }
                    if (scores.better(SPACE_WORLD.points) >= 0) {
                        gc.strokeText("Nouveau meilleur score, B R A V O!!", gc.screenSize().x().half().asDouble(), gc.screenSize().y().half().asDouble() + 20);
                        //getName();
                        //SPACE_WORLD.points = 0;
                    }
                    int y = gc.screenSize().y().half().asInt() + 20;
                    for (Score score : scores.scores()) {
                        gc.strokeText(score.name(), gc.screenSize().x().half().asDouble(), y);
                        gc.strokeText(score.points() + "pts", gc.screenSize().x().half().asDouble() + 200, y);
                        y += 20;
                    }
                }
            }
        }.start();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private static final Scores scores = new Scores();


    private static void getName() {
        engine.stop();
        final TextInputDialog dialog = new TextInputDialog("Le pilote");
        dialog.setTitle("Record battu!");
        dialog.setHeaderText("Nouveau meilleur score, B R A VO!");
        dialog.setContentText("Ton nom: ");
        dialog.setOnHidden(evt -> {
            scores.add(SPACE_WORLD.points, dialog.getResult());
            engine.start();
        });
        dialog.show();
    }
}
