package net.esyo.tileengine.rtype;

import javafx.scene.paint.Color;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.RenderListener;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.effects.AbstractParticle;

/**
 *
 * @author rpetit
 */
public class RayParticle extends AbstractParticle implements RenderListener {

    private double size = 4;
    private double angle;//radian
    private final Vect2 screenSize;
    private Color color  = Color.WHITE;

    public RayParticle(final Vect2 screenSize) {
        super();
        this.screenSize = screenSize.copy();
        super.addRenderListener(this);
    }

    @Override
    public void onRender(final Graphics gc) {
        gc.setStroke(color);
        gc.strokeLine(position(), Vect2.fromRadian(angle).mul(size).add(position()));
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        this.position().add(Vect2.fromRadian(angle).mul(velocity()));
        this.velocity().add(this.acceleration());
    }

    public RayParticle size(final double size) {
        this.size = size;
        return this;
    }

    public double size(){
        return this.size;
    }

    /**
     *
     * @param angle radian
     * @return
     */
    public RayParticle angle(final double angle) {
        this.angle = angle;
        return this;
    }

    /**
     *
     * @return angle radian
     */
    public double angle(){
        return this.angle;
    }

    public RayParticle color(final Color color){
        this.color = color;
        return this;
    }

    public Color color(){
        return this.color;
    }

    public boolean isAlive() {
        return isVisible() && position().x().positive() && position().x().lowerThan(screenSize.x()) && position().y().positive() && position().y().lowerThan(screenSize.y());
    }

    @Override
    public boolean beforeRender(final Graphics gc) {
        return true;
    }
}
