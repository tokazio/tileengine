package net.esyo.tileengine.rtype;

import net.esyo.tileengine.engine.core.Movable;
import net.esyo.tileengine.engine.core.Positionable;

/**
 *
 * @author rpetit
 */
public interface PositionableMovable extends Positionable, Movable {

}
