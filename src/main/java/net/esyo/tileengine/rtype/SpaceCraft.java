package net.esyo.tileengine.rtype;

import javafx.scene.input.KeyEvent;
import net.esyo.tileengine.engine.ObjectSet;
import net.esyo.tileengine.engine.core.*;

import java.util.logging.Level;
import java.util.logging.Logger;

import static javafx.scene.input.KeyCode.*;

/**
 *
 * @author rpetit
 */
public class SpaceCraft extends Craft implements SpaceItem, OnKey {

    private static final ObjectSet<Class<?>> UNCOLLIDE = ObjectSet.of(Bullet.class, SpaceCraft.class);

    private final Reactor reactor = new Reactor(60);

    Weapon weapon;
    Eye eyeUp;
    Eye eyeDown;

    private double ACC = 0.1;
    private double SPEED = 0.5;
    private double MAX_SPEED = 8;
    private int FIRE_RATE = 100;
    boolean firing = false;
    private long lastFire = 0;

    public SpaceCraft(final Producer producer) {
        super("src/main/resources/rtype/Assets/Game/Starship/starship1tileset.png", Vect2.of(32, 14), producer);
        super.defaultTileCode(1);
        super.addKeyFrames("NORMAL", new KeyFrames(1));
        super.addKeyFrames("LEFT", new KeyFrames(1));
        super.addKeyFrames("RIGHT", new KeyFrames(1));
        super.addKeyFrames("DOWN", new KeyFrames(2));
        super.addKeyFrames("UP", new KeyFrames(0));
        try {
            super.selectKeyFrames("NORMAL");
        } catch (AnimatedSpriteException ex) {
            Logger.getLogger(SpaceCraft.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void render(final Graphics gc) {
        super.render(gc);
        reactor.render(gc);
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        super.update(screenSize, elapsedTime);

        Util.boundTo(this, Rect2.of(0, 0, screenSize));
        Util.constrainVelocity(this, MAX_SPEED);

        //Explode on floor
        //todo replace with background collisions
        if (position().y().add(17).greaterOrEquals(screenSize.y())) {
            explode();
        }

        reactor.position(Vect2.of(position().x(), position().y().add(8)));
        reactor.update(screenSize, elapsedTime);

        if (firing && System.currentTimeMillis() - lastFire > FIRE_RATE) {
            
            if(weapon==null || !weapon.isAttached()){
            final Bullet bullet = new Bullet(screenSize, this)
                    .velocity(Vect2.of(SPEED * 4, SPEED * 4))
                    .acceleration(Vect2.of(ACC * 1.5, ACC * 1.5));
            producer.produce(bullet.position(position().copy().add(Vect2.of(32, 9))));
            lastFire = System.currentTimeMillis();
            }
        }
    }

    public void startFire() {
        if (isAlive()) {
            firing = true;
        }
    }

    public void stopFire() {
        firing = false;
    }

    @Override
    public void onExplode() {
        reactor.stop();
        firing = false;
    }

    public void goUp() {
        if (!"UP".equals(this.selectedKeyFramesName())) {
            try {
                this.selectKeyFrames("UP");
                this.velocity().y(-SPEED);
                this.acceleration().y(-ACC);
                startReactor();
            } catch (AnimatedSpriteException ex) {

            }
        }
    }

    public void goDown() {
        if (!"DOWN".equals(this.selectedKeyFramesName())) {
            try {
                this.selectKeyFrames("DOWN");
                this.velocity().y(SPEED);
                this.acceleration().y(ACC);
                startReactor();
            } catch (AnimatedSpriteException ex) {

            }
        }
    }

    public void goNormal(KeyEvent k) {
        if (k.getCode().name().equals(this.selectedKeyFramesName())) {
            try {
                this.selectKeyFrames("NORMAL");
                this.velocity().asZero();
                this.acceleration().asZero();
                startReactor();
            } catch (AnimatedSpriteException ex) {

            }
        }
    }

    public void goLeft() {
        if (!"LEFT".equals(this.selectedKeyFramesName())) {
            try {
                this.selectKeyFrames("LEFT");
                this.velocity().x(-SPEED);
                this.acceleration().x(-ACC);
                stopReactor();
            } catch (AnimatedSpriteException ex) {

            }
        }
    }

    public void goRight() {
        if (!"RIGHT".equals(this.selectedKeyFramesName())) {
            try {
                this.selectKeyFrames("RIGHT");
                this.velocity().x(SPEED);
                this.acceleration().x(ACC);
                startReactor();
            } catch (AnimatedSpriteException ex) {

            }
        }
    }

    @Override
    public boolean intersect(final Collidable other) {
        if (UNCOLLIDE.contains(other.getClass())) {
            return false;
        }
        return super.intersect(other) && getBounds().intersect(other.getBounds());
    }

    @Override
    public Rect2 getBounds() {
        return Rect2.of(position(), 32, 16);
    }

    @Override
    public void onKeyUp(final KeyEvent k) {
        if (k.getCode() == SPACE) {
            stopFire();
        } else {
            goNormal(k);
        }
        if (k.getCode() == B) {
            if (weapon.isAttached()) {
                weapon.detach();
            } else {
                weapon.attach();
            }
        }
    }

    @Override
    public void onKeyPress(final KeyEvent k) {
        if (k.getCode() == SPACE) {
            startFire();
        }
        if (k.getCode() == UP) {
            goUp();
        }
        if (k.getCode() == DOWN) {
            goDown();
        }
        if (k.getCode() == LEFT) {
            goLeft();
        }
        if (k.getCode() == RIGHT) {
            goRight();
        }
    }

    @Override
    public String toString() {
        return "SpaceCraft";
    }

    @Override
    public int power() {
        return -1;//not used
    }

    @Override
    public SpaceCraft hit(final Collidable c) {
        explode();
        return this;
    }

    private void startReactor() {
        if (isAlive()) {
            reactor.start();
        }
    }

    private void stopReactor() {
        if (isAlive()) {
            reactor.stop();
        }
    }

    void setEyeUp(Eye eyeUp) {
        this.eyeUp = eyeUp;
        eyeUp.firing = this.firing;
    }

    void setEyeDown(Eye eyeDown) {
        this.eyeDown = eyeDown;
        eyeDown.firing = this.firing;
    }

    @Override
    public void onHit() {
        //nothing
    }

}
