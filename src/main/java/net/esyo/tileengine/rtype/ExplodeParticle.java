package net.esyo.tileengine.rtype;

import javafx.scene.paint.Color;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.effects.AbstractLivingParticle;

/**
 *
 * @author rpetit
 */
public class ExplodeParticle extends AbstractLivingParticle {

    private double size = 10;
    private Color color = Color.RED;
    private double opacityV = 1;
    private double sizeV = 1;

    public ExplodeParticle() {
        super();
    }

    @Override
    public void render(Graphics gc) {
        if (isAlive()) {
            gc.setFill(color, opacityV);
            double s = size * sizeV;
            //todo fillCircle from center with radius
            gc.fillOval(position().x().asDouble() - s, position().y().asDouble() - s, s, s);
        }
    }


    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        super.update(screenSize, elapsedTime);
        if (isAlive()) {
            opacityV = timeToLive / lifeTime;
            sizeV = timeToLive / lifeTime;
        }
    }


    public ExplodeParticle color(Color color) {
        this.color = color;
        return this;
    }

    public ExplodeParticle size(double d) {
        this.size = d;
        return this;
    }

    @Override
    public ExplodeParticle rebirth() {
        super.rebirth();
        size = 10;
        color = Color.WHITE;
        opacityV = 1;
        sizeV = 1;
        return this;
    }

}
