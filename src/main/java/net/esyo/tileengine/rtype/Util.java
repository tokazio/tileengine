package net.esyo.tileengine.rtype;

import net.esyo.tileengine.engine.core.Movable;
import net.esyo.tileengine.engine.core.Rect2;

/**
 *
 * @author romainpetit
 */
public final class Util {

    private Util() {
        //hide
    }

    public static void constrainVelocity(final Movable s, final double max) {
        if (s.velocity().x().greaterThan(max)) {
            s.velocity().x(max);
        }
        if (s.velocity().x().lowerThan(-max)) {
            s.velocity().x(-max);
        }

        if (s.velocity().y().greaterThan(max)) {
            s.velocity().y(max);
        }
        if (s.velocity().y().lowerThan(-max)) {
            s.velocity().y(-max);
        }
    }

    public static void boundTo(final PositionableMovable s, final Rect2 screenSize) {
        if (s.position().y().lowerOrEquals(screenSize.top())) {
            s.position().y(1);
            s.velocity().y(0);
            s.acceleration().y(0);
        }

        if (s.position().y().add(17).greaterOrEquals(screenSize.bottom())) {
            s.position().y(screenSize.right().sub(17));
            s.velocity().y(0);
            s.acceleration().y(0);
        }

        if (s.position().x().lowerOrEquals(screenSize.left())) {
            s.position().x(1);
            s.velocity().x(0);
            s.acceleration().x(0);
        }

        if (s.position().x().add(32).greaterOrEquals(screenSize.right())) {
            s.position().x(screenSize.right().sub(32));
            s.velocity().x(0);
            s.acceleration().x(0);
        }
    }
}
