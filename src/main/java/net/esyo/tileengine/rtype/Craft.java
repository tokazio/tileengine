package net.esyo.tileengine.rtype;

import javafx.scene.paint.Color;
import net.esyo.tileengine.engine.core.*;
import net.esyo.tileengine.engine.core.utils.ImageUtils;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rpetit
 */
public abstract class Craft extends AnimatedSprite implements Collidable, Explodable, SpaceItem {

    protected final Producer producer;
    protected final ExplodeParticleEmitter exploder = new ExplodeParticleEmitter(2000);

    private Vect2 velocity = Vect2.zero();
    private Vect2 acceleration = Vect2.zero();

    private boolean exploded;

    protected TileSet whiteTileSet;
    private long hitted;
    private int life = 500;
    private int maxLife = 500;

    public Craft(final String tileSetFileName, final Vect2 tileSize, final Producer producer) {
        super(loadTileSet(tileSetFileName, tileSize));
        white();
        this.producer = producer;
    }

    @Override
    public TileSet tileSet() {
        if (System.currentTimeMillis() - this.hitted < 100) {
            return this.whiteTileSet;
        }
        return this.tileSet;
    }

    private void white() {
        try {
            this.whiteTileSet = new TileSet(ImageUtils.white(this.tileSet.image()), this.tileSet.size());
        } catch (TileSetException ex) {
            Logger.getLogger(SpaceCraft.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static TileSet loadTileSet(final String filename, final Vect2 tileSize) {
        try {
            return new TileSet(filename, tileSize);
        } catch (FileNotFoundException | TileSetException ex) {
            Logger.getLogger(SpaceCraft.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


    @Override
    public void render(final Graphics gc) {
        if (isAlive()) {
            super.render(gc);
            if (System.currentTimeMillis() - this.hitted < 250) {
                final double s = Math.max(getBounds().width().asDouble(), getBounds().height().asDouble())*1.5;
                final Rect2 r = Rect2.of(position().x().add(16).sub(s/2).asDouble(), position().y().add(8).sub(s/2).asDouble(),s,s);
                gc.setLineWidth(1);
                gc.setStroke(Color.GREEN);
                gc.strokeArc(r,90,(life/(double) maxLife)*-360);
            }
        }
        exploder.render(gc);
    }

    public int life(){
        return this.life;
    }

    public Craft life(int life){
        this.life = life;
        this.maxLife = life;
        return this;
    }

    public boolean isAlive() {
        return !exploded;
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        if (isAlive()) {
            position().add(velocity);
            velocity().add(acceleration);
        }
        exploder.update(screenSize, elapsedTime);
    }

    @Override
    public void explode() {
        if (isAlive()) {
            this.exploded = true;
            onExplode();
            this.exploder.position(position().add(Vect2.of(17, 12)));
            this.exploder.explode();
        }
    }

    @Override
    public boolean isExploded() {
        return this.exploded;
    }

    @Override
    public boolean canExplode() {
        return !isExploded() && isAlive();
    }

    public abstract void onExplode();

    @Override
    public boolean canRemove(){
        return isExploded() && !isAlive() && exploder.finished();
    }

    @Override
    public String toString() {
        return "Craft";
    }

    @Override
    public boolean intersect(final Collidable other) {
        return other != this && isAlive() && !isExploded();
    }

    @Override
    public Craft velocity(final Vect2 velocity) {
        this.velocity = velocity.copy();
        return this;
    }

    @Override
    public Craft acceleration(final Vect2 acceleration) {
        this.acceleration = acceleration;
        return this;
    }

    @Override
    public Vect2 velocity(){
        return this.velocity;
    }

    @Override
    public Vect2 acceleration(){
        return this.acceleration;
    }

    public abstract void onHit();

    @Override
    public Craft hit(final Collidable c) {
        this.hitted = System.currentTimeMillis();
        life-= c.power();
        onHit();
        return this;
    }

}
