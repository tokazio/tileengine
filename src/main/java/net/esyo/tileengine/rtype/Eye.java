package net.esyo.tileengine.rtype;

import static javafx.scene.input.KeyCode.SPACE;
import javafx.scene.input.KeyEvent;
import net.esyo.tileengine.engine.ObjectSet;
import net.esyo.tileengine.engine.core.Collidable;
import net.esyo.tileengine.engine.core.KeyFrames;
import net.esyo.tileengine.engine.core.OnKey;
import net.esyo.tileengine.engine.core.Rect2;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author rpetit
 */
public class Eye extends Craft implements OnKey {

    private static final ObjectSet<Class<?>> UNCOLLIDE = ObjectSet.of(Bullet.class, SpaceCraft.class, Weapon.class, Eye.class);

    private final Craft owner;

    private boolean reversed;
    private double ACC = 0.1;
    private double SPEED = 0.5;
    private int FIRE_RATE = 100;
    boolean firing = false;
    private long lastFire = 0;

    public Eye(final Producer producer, final Craft owner) {
        super("src/main/resources/rtype/Assets/Game/Starship/r-typesheet3.gif", Vect2.of(17, 14), producer);
        this.owner = owner;
        super.addKeyFrames("NORMAL", new KeyFrames(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)).duration(50).play(true);
    }

    @Override
    public Rect2 getBounds() {
        return Rect2.of(position().copy(), 17, 14);
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        super.update(screenSize, elapsedTime);
        //todo stick to owner x,y with inertia
        //todo collision with floor
        position(Vect2.of(owner.position().x().add(7), reversed ? owner.position().y().add(16 + 20) : owner.position().y().sub(14 + 20)));
        if (firing && System.currentTimeMillis() - lastFire > FIRE_RATE) {
            final Bullet bullet = new Bullet(screenSize, this)
                    .velocity(Vect2.of(SPEED * 4, SPEED * 4))
                    .acceleration(Vect2.of(ACC * 1.5, ACC * 1.5));
            producer.produce(bullet.position(position().copy().add(Vect2.of(17, 7)))
            );
            lastFire = System.currentTimeMillis();
        }
    }

    @Override
    public void explode() {
        //can't explode
    }

    @Override
    public void onExplode() {
        //can't explode
    }

    public Eye reverse() {
        this.reversed = true;
        this.keyFrames("NORMAL").reverse();
        return this;
    }

    @Override
    public boolean intersect(final Collidable other) {
        if (UNCOLLIDE.contains(other.getClass())) {
            return false;
        }
        return super.intersect(other) && getBounds().intersect(other.getBounds());
    }

    @Override
    public boolean isExploded() {
        //can't explode
        return false;
    }

    @Override
    public boolean canExplode() {
        //can't explode
        return false;
    }

    @Override
    public void onKeyUp(final KeyEvent k) {
        if (k.getCode() == SPACE) {
            stopFire();
        }
    }

    @Override
    public void onKeyPress(final KeyEvent k) {
        if (k.getCode() == SPACE) {
            startFire();
        }
    }

    public void startFire() {
        if (owner.isAlive()) {
            firing = true;
        }
    }

    public void stopFire() {
        firing = false;
    }

    @Override
    public int power() {
        return 1000;
    }

    @Override
    public Eye hit(final Collidable c) {
        //do nothing
        return this;
    }

    @Override
    public void onHit() {
        //nothing
    }
}
