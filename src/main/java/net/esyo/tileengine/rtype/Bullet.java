package net.esyo.tileengine.rtype;

import net.esyo.tileengine.engine.ObjectSet;
import net.esyo.tileengine.engine.core.Collidable;
import net.esyo.tileengine.engine.core.Explodable;
import net.esyo.tileengine.engine.core.Rect2;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author romainpetit
 */
public class Bullet extends RayParticle implements Collidable, Explodable, SpaceItem {

    private static final ObjectSet<Class<?>> UNCOLLIDE = ObjectSet.of(SpaceCraft.class,Weapon.class, Bullet.class);

    private final Craft owner;

    public Bullet(final Vect2 screenSize, final Craft owner) {
        super(screenSize);
        this.owner = owner;
    }

    @Override
    public boolean intersect(final Collidable other) {
        if (UNCOLLIDE.contains(other.getClass())) {
            return false;
        }
        return other != this && isVisible() && getBounds().intersect(other.getBounds());
    }

    @Override
    public void explode() {
        visible(false);
    }

    @Override
    public Rect2 getBounds() {
        return Rect2.of(position().copy(), size(), 1);
    }

    @Override
    public boolean isExploded() {
        return !this.isVisible();
    }

    @Override
    public boolean canExplode() {
        return true;
    }

    @Override
    public boolean canRemove() {
        return !isAlive();
    }

    @Override
    public int power() {
        return 500;
    }

    @Override
    public Bullet hit(final Collidable c) {
        explode();
        return this;
    }
}
