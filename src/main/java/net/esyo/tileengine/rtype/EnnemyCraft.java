package net.esyo.tileengine.rtype;

import net.esyo.tileengine.engine.ObjectSet;
import net.esyo.tileengine.engine.core.Collidable;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.KeyFrames;
import net.esyo.tileengine.engine.core.Rect2;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author rpetit
 */
public class EnnemyCraft extends Craft {

    private static int nb = 0;

    private static final ObjectSet<Class<?>> UNCOLLIDE = ObjectSet.of(RedBullet.class, EnnemyCraft.class);
    private static final int FIRE_RATE = 5000;

    private final int FIRE_DECAY = (int) (Math.random() * FIRE_RATE) + FIRE_RATE / 3;
    private final Craft target;

    private boolean firing = false;
    private long lastFire = 0;
    private final long num;

    public EnnemyCraft(final Producer producer, final Craft target) {
        super("src/main/resources/rtype/Assets/Game/r-typesheet5.gif", Vect2.of(33, 24), producer);
        this.num = nb++;
        this.target = target;
        super.addKeyFrames("NORMAL", new KeyFrames(0, 1, 2, 3, 4, 5, 6, 7)).duration(100).pause((int) (Math.random() * 2000)).play(true);
        lastFire = System.currentTimeMillis();
    }

    @Override
    public void render(final Graphics gc) {
        super.render(gc);
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        super.update(screenSize, elapsedTime);

        //start fire after decay
        if (!firing && System.currentTimeMillis() - lastFire > FIRE_DECAY) {
            firing = true;
            lastFire = System.currentTimeMillis() - FIRE_RATE;
        }

        //timed fire
        if (firing && System.currentTimeMillis() - lastFire > FIRE_RATE) {
            producer.produce(new RedBullet(screenSize, this, target).position(position().copy().add(Vect2.of(0, 10))));
            lastFire = System.currentTimeMillis();
        }
    }

    public void startFire() {
        if (isAlive()) {
            firing = true;
        }
    }

    public void stopFire() {
        firing = false;
    }

    @Override
    public void onExplode() {
        SpaceWorld.addPoints(400);
    }

    @Override
    public boolean intersect(final Collidable other) {
        if (UNCOLLIDE.contains(other.getClass())) {
            return false;
        }
        return super.intersect(other) && getBounds().intersect(other.getBounds());
    }

    @Override
    public Rect2 getBounds() {
        return Rect2.of(position(), 33, 24);
    }
    @Override
    public String toString() {
        return "EnnemyCraft #" + num;
    }

    @Override
    public int power() {
        return 1000;
    }

    /*
    @Override
    public EnnemyCraft hit(final Collidable c) {
        super.hit(c);

        if (this.life() <= 0 && canExplode()) {
            explode();
        }
        return this;
    }
    */

    @Override
    public void onHit() {
        SpaceWorld.addPoints(100);
        if (this.life() <= 0 && canExplode()) {
            explode();
        }
    }
}
