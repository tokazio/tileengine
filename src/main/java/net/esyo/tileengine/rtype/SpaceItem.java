package net.esyo.tileengine.rtype;

import net.esyo.tileengine.engine.core.Collectable;
import net.esyo.tileengine.engine.core.Renderable;
import net.esyo.tileengine.engine.core.Updatable;

/**
 *
 * @author romainpetit
 */
public interface SpaceItem extends PositionableMovable, Renderable, Updatable, Collectable {

}
