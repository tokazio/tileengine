package net.esyo.tileengine.rtype;

import javafx.scene.paint.Color;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Pt;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.effects.AbstractParticle;

/**
 *
 * @author romainpetit
 */
public class ArcRayParticle extends AbstractParticle implements SpaceItem {

    private double size = 1;
    private final Vect2 screenSize;
    private final Vect2 start;

    private final Craft owner;

    public ArcRayParticle(final Vect2 screenSize, final Vect2 start, final Craft owner) {
        super();
        this.start = start;
        this.screenSize = screenSize.copy();
        this.owner = owner;
    }

    private Pt x = Pt.of(0);
    
    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        x.add(velocity().x());
        position(start.copy().add(Vect2.of(x.asDouble()*30,40*Math.sin(x.asDouble()*30))));        
    }

    @Override
    public void render(final Graphics gc) {
        gc.setStroke(Color.VIOLET);
        if(parent!=null){
            /*
            
            var counter = 0, x=0,y=180;


//100 iterations
var xAmp = 10;
var yAmp = xAmp * 10;
var increase = 90/180*Math.PI/10;
for(i=0; i<=360; i+=xAmp){
    
     ctx.moveTo(x,y);
    x = i;
    y =  180 - Math.sin(counter) * yAmp;
counter += increase;
     
    ctx.lineTo(x,y);
    ctx.stroke();
    //alert( " x : " + x + " y : " + y + " increase : " + counter ) ;

            
            Vect2 a = start.copy();
            for(int i = position().x().asInt();i<parent.position().x().asInt();i++){
                final Vect2 b = Vect2.of(i, );
                gc.strokeLine(a,b);
                a = b;
            }
        }
        */
            gc.strokeLine(start,position());
        }
    }

    @Override
    public boolean canRemove() {
        return false;
    }

    private ArcRayParticle parent;
    
    public ArcRayParticle last(ArcRayParticle last) {
        this.parent = last;
        return this;
    }
    
}
