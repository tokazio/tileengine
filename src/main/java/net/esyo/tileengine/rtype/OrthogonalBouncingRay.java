package net.esyo.tileengine.rtype;

import java.util.ArrayList;
import java.util.List;
import net.esyo.tileengine.engine.ObjectSet;
import net.esyo.tileengine.engine.core.Collidable;
import net.esyo.tileengine.engine.core.Explodable;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Movable;
import net.esyo.tileengine.engine.core.Positionable;
import net.esyo.tileengine.engine.core.Rect2;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.effects.AbstractParticle;
import static net.esyo.tileengine.engine.core.utils.AngleUtils.*;

/**
 *
 * @author romainpetit
 */
public class OrthogonalBouncingRay extends AbstractParticle implements Collidable, Explodable, SpaceItem {

    private static final ObjectSet<Class<?>> UNCOLLIDE = ObjectSet.of(SpaceCraft.class,Weapon.class, Bullet.class, RedBullet.class, OrthogonalBouncingRay.class);

    private final List<BouncingRayParticle> particles = new ArrayList<>();
    private final double angle;//radian
    private double lifeV = 1000;//must be same as life at initialisation (->100%)
    private double life = 1000;

    private final Vect2 screenSize;

    private final Craft owner;

    /**
     *
     * @param owner
     * @param angle radian
     */
    public OrthogonalBouncingRay(final Vect2 screenSize, final Craft owner, final double angle) {
        super();
        this.screenSize = screenSize.copy();
        this.owner = owner;
        this.angle = angle;
        this.particles.add(new BouncingRayParticle(angle, 1));
    }

    /**
     * Be carrefull, this set all the child positions
     *
     * @param pos
     * @return
     */
    @Override
    public <T extends Positionable> T position(final Vect2 pos) {
        super.position(pos);
        this.particles.forEach(p -> p.position(pos));
        return (T) this;
    }

    /**
     * Be carrefull, this set all the child velocities
     *
     * @param pos
     * @return
     */
    @Override
    public <T extends Movable> T velocity(final Vect2 pos) {
        super.velocity(pos);
        this.particles.forEach(p -> p.velocity(pos));
        return (T) this;
    }

    @Override
    public void render(final Graphics gc) {
        this.particles.forEach(p -> p.render(gc));
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        final int s = this.particles.size();
        for (int i = s - 1; i >= 0; i--) {
            if (!this.particles.isEmpty()) {
                final BouncingRayParticle b = this.particles.get(i);
                b.update(screenSize, elapsedTime);
                if (!b.isVisible()) {
                    this.particles.remove(b);
                }
                if (!b.isBouncing()) {
                    //hz floor
                    if (b.end().y().greaterThan(screenSize.y().sub(5))) {
                        final double rad = b.asVector().angle();
                        add(b, ((rad > A90 && rad < A180) || (rad > -A90 && rad < A0)) ? A90 : -A90);
                    }/* else {
                    //vt
                    if (b.end().x().greaterThan(other.getBounds().left()) || b.end().x().lowerThan(other.getBounds().right())) {
                        final double rad = b.asVector().angle();
                        add(b, ((rad > A0 && rad < A90) || (rad > A180 && rad < A270)) ? A90 : -A90);
                    }
                }*/
                }
            }
        }
        visible(!particles.isEmpty());
    }

    /**
     *
     * @param parentParticle
     * @param angle radian
     */
    private void add(final BouncingRayParticle parentParticle, final double angle) {
        if (parentParticle != null) {
            final BouncingRayParticle newParticle = new BouncingRayParticle(this.angle, this.lifeV / this.life);
            parentParticle.bouncing(parentParticle.end(), newParticle);
            newParticle.velocity(velocity());
            newParticle.position(parentParticle.end());
            newParticle.angle((parentParticle.angle() + angle));
            this.particles.add(newParticle);
            if (hasLimitedLife()) {
                this.lifeV -= 100;
                if (this.lifeV <= 0) {
                    this.particles.clear();
                }
            }
        }
    }

    public OrthogonalBouncingRay life(final double life) {
        this.life = life;
        return this;
    }

    boolean hasLimitedLife() {
        return true;
    }

    @Override
    public boolean intersect(final Collidable other) {
        if (!isVisible() || other == this || UNCOLLIDE.contains(other.getClass()) || !getBounds().intersect(other.getBounds())) {
            return false;
        }
        final int s = this.particles.size();
        for (int i = s - 1; i >= 0; i--) {
            if (!this.particles.isEmpty()) {
                final BouncingRayParticle b = this.particles.get(i);
                if (!b.isBouncing()) {
                    //hz
                    if (b.end().y().greaterThan(other.getBounds().top()) || b.end().y().lowerThan(other.getBounds().bottom())) {
                        final double rad = b.asVector().angle();
                        add(b, ((rad > A90 && rad < A180) || (rad > -A90 && rad < A0)) ? A90 : -A90);
                    } else {
                        //vt
                        if (b.end().x().greaterThan(other.getBounds().left()) || b.end().x().lowerThan(other.getBounds().right())) {
                            final double rad = b.asVector().angle();
                            add(b, ((rad > A0 && rad < A90) || (rad > A180 && rad < A270)) ? A90 : -A90);
                        }
                    }
                }
            }
        }
        return true;
    }

    @Override
    public Rect2 getBounds() {
        if (!this.particles.isEmpty()) {
            final Vect2 end = this.particles.get(this.particles.size() - 1).end();
            return Rect2.of(end, Vect2.of(1));
        }
        return Rect2.of(position(), Vect2.of(1));
    }

    @Override
    public int power() {
        return (int) this.lifeV;
    }

    @Override
    public <T extends Collidable> T hit(final Collidable c) {
        return (T) this;
    }

    @Override
    public boolean canRemove() {
        return !isVisible();
    }

    @Override
    public void explode() {

    }

    @Override
    public boolean isExploded() {
        return false;
    }

    @Override
    public boolean canExplode() {
        return false;
    }

    @Override
    public boolean isVisible() {
        if (this.particles.isEmpty()) {
            return false;
        }
        final Vect2 start = this.particles.get(0).position();
        return super.isVisible() && lifeV > 0 && start.x().positive() && start.y().positive() && start.x().lowerThan(screenSize.x()) && start.y().lowerThan(screenSize.y());
    }
}
