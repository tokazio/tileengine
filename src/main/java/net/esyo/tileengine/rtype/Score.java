package net.esyo.tileengine.rtype;

/**
 *
 * @author rpetit
 */
public class Score {

    private final String name;
    private final long points;

    public Score(final long points, final String name) {
        this.points = points;
        this.name = name;
    }

    public String name() {
        return name;
    }

    public long points() {
        return points;
    }

}
