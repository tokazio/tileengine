package net.esyo.tileengine.rtype;

import javafx.scene.paint.Color;
import net.esyo.tileengine.engine.ObjectSet;
import net.esyo.tileengine.engine.core.Collidable;
import net.esyo.tileengine.engine.core.Explodable;
import net.esyo.tileengine.engine.core.Rect2;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author romainpetit
 */
public class RedBullet extends CircleParticle implements Collidable, Explodable, SpaceItem {

    private static final ObjectSet<Class<?>> UNCOLLIDE = ObjectSet.of(EnnemyCraft.class);//RedBullet.class

    private final Craft owner;
    private final Craft target;

    public RedBullet(final Vect2 screenSize, final Craft owner, final Craft target) {
        super(screenSize);
        this.owner = owner;
        this.target = target;
        super.color(Color.RED);
        //todo go to y target
        super.velocity(Vect2.of(-2, 0));
        super.acceleration(Vect2.of(-0.005, 0));
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        //trop bas
        if (target.position().y().greaterThan(position().y()) && acceleration().y().zero()) {
            acceleration().y(0.01);
        }
        //trop haut
        if (target.position().y().lowerThan(position().y()) && acceleration().y().zero()) {
            acceleration().y(-0.01);
        }
        super.update(screenSize, elapsedTime);
    }


    @Override
    public boolean intersect(final Collidable other) {
        if (UNCOLLIDE.contains(other.getClass())) {
            return false;
        }
        return other != this && isVisible() && getBounds().intersect(other.getBounds());
    }

    @Override
    public void explode() {
        visible(false);
    }

    @Override
    public Rect2 getBounds() {
        return Rect2.of(position(), size(), 1);
    }

    @Override
    public boolean canRemove() {
        return !isAlive();
    }

    @Override
    public boolean isExploded() {
        return !isVisible();
    }

    @Override
    public boolean canExplode() {
        return true;
    }

    @Override
    public int power() {
        return -1;//not used
    }

    @Override
    public RedBullet hit(final Collidable c) {
        explode();
        return this;
    }
}
