package net.esyo.tileengine.rtype;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import net.esyo.tileengine.engine.core.Collidable;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.OnKey;
import net.esyo.tileengine.engine.core.Renderable;
import net.esyo.tileengine.engine.core.Updatable;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author romainpetit
 */
public class SpaceWorld implements Renderable, Updatable, OnKey {

    private static final double FLOOR = 30;

    public static long points = 0;
    private int col = 1;

    private final List<SpaceItem> items = new CopyOnWriteArrayList<>();
    private final Producer producer = c -> items.add(c);

    private final StarEmitter stars;

    private final Vect2 screenSize;
    private boolean finished;

    public SpaceWorld(final Vect2 screenSize) {
        this.screenSize = screenSize;
        stars = new StarEmitter(screenSize, 100);
        newGame(col, 13);
    }

    private SpaceCraft craft;

    private void newGame(final int col, final int row) {
        clear();
        craft = new SpaceCraft(producer).position(Vect2.of(100, screenSize.y().half()));
        items.add(craft);
        items.add(new Weapon(producer, craft));
        finished = false;
        SpaceWorld.points = 0;
        e = 1;
        f = 10000;
        ennemyLife = 500;
    }

    @Override
    public void render(final Graphics gc) {

        stars.render(gc);

        for (int i = 1; i < FLOOR; i++) {
            gc.setStroke(Color.GRAY, 1 - (i / FLOOR));
            gc.strokeLine(0, gc.screenSize().y().sub(i).asDouble(), gc.screenSize().x().asDouble(), gc.screenSize().y().sub(i).asDouble());
        }
        items.forEach(it -> it.render(gc));
        gc.setStroke(Color.ORANGE);
        gc.strokeText("x" + items.size(), 50, 20);
        gc.strokeText(points + "pts", screenSize.x().asDouble() - 100, 20);
        gc.strokeText(">" + f, screenSize.x().asDouble() - 100, 40);
    }

    private int e = 1;
    private int f = 10000;
    private int ennemyLife = 500;
    private long lastEnnemyUpgrade = System.currentTimeMillis();

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {

        stars.update(screenSize, elapsedTime);

        if (System.currentTimeMillis() - lastEnnemyUpgrade > 10000) {
            ennemyLife += 250;
            lastEnnemyUpgrade = System.currentTimeMillis();
        }

        if (points > f) {
            f *= 2;
            switch (e) {
                case 1:
                    e++;
                    final Eye eyeDown = new Eye(producer, craft).reverse();
                    items.add(eyeDown);
                    craft.setEyeDown(eyeDown);
                    break;
                case 2:
                    e++;
                    final Eye eyeUp = new Eye(producer, craft);
                    items.add(eyeUp);
                    craft.setEyeUp(eyeUp);
                    break;
                /*
                case 3:
                    e++;
                    items.add(new Weapon(producer, craft));
                    break;
                 */
            }
        }

        if ((int) (Math.random() * 80) % 80 == 0) {
            final EnnemyCraft e = new EnnemyCraft(producer, craft)
                    .position(Vect2.of(screenSize.x(), Math.random() * (screenSize.y().asDouble() - 80) + 80));
            items.add(e.life(ennemyLife).velocity(Vect2.of(Math.random() * -2 - 1, 0)));
        }

        items.forEach(it -> it.update(screenSize, elapsedTime));
        collide();
        clean();
    }

    private void clean() {
        items.forEach(it -> {
            if (it.canRemove()) {
                items.remove(it);
                if (it instanceof SpaceCraft) {
                    this.finished = true;
                }
            }
        });
        /*
        if (items.size() == 4) {
            this.finished = true;
        }
         */
    }

    private void clear() {
        items.forEach(it -> {
            items.remove(it);
        });
    }

    private void collide() {
        for (SpaceItem it : items) {
            if (it instanceof Collidable) {
                if (itemsCollideAgainst((Collidable) it)) {
                    return;
                }
            }
        }
    }

    private boolean itemsCollideAgainst(final Collidable c) {
        for (SpaceItem it : items) {
            if (it != c && it instanceof Collidable) {
                final Collidable against = (Collidable) it;
                if (against.intersect(c) && c.intersect(against)) {
                    //HIT EnnemyCraft(class net.esyo.tileengine.rtype.EnnemyCraft, power=1000) against net.esyo.tileengine.rtype.Bullet@50618+(class net.esyo.tileengine.rtype.Bullet, power=500)
                    //System.out.println("HIT " + c + "(" + c.getClass() + ", power=" + c.power() + ") against " + against + "+(" + against.getClass() + ", power=" + against.power() + ")");
                    c.hit(against);
                    against.hit(c);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void onKeyPress(final KeyEvent k) {
        if (isFinished() && k.getCode() == KeyCode.ENTER) {
            if (win()) {
                col++;
            }
            newGame(col, 13);
        }
        items.forEach(it -> {
            if (it instanceof OnKey) {
                ((OnKey) it).onKeyPress(k);
            }
        });
    }

    @Override
    public void onKeyUp(final KeyEvent k) {
        items.forEach(it -> {
            if (it instanceof OnKey) {
                ((OnKey) it).onKeyUp(k);
            }
        });
    }

    public boolean isFinished() {
        return this.finished;
    }

    //craft + weapon + eya up + eye down = 4
    public boolean win() {
        return items.size() == 4;
    }

    public static void addPoints(final int i) {
        points += i;
    }

}
