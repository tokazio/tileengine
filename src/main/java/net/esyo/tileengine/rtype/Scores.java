package net.esyo.tileengine.rtype;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author rpetit
 */
public class Scores {

    private static final LinkedList<Score> scores = new LinkedList<>();

    public int better(final long points) {
        sort();
        if (scores.size() < 10) {
            return scores.size();
        }
        for (int i = scores.size(); i > 0; i--) {
            if (points > scores.get(i).points()) {
                return i;
            }
        }
        return -1;
    }

    private void sort() {
        scores.sort((o1, o2) -> Long.valueOf(o1.points()).compareTo(o2.points()));
    }

    public void add(long points, String name) {
        scores.add(new Score(points, name));
        sort();
        if (scores.size() > 10) {
            scores.removeLast();
        }
        System.out.println("after add: " + scores);
    }

    public List<Score> scores() {
        return new ArrayList<>(this.scores);
    }
}
