package net.esyo.tileengine.rtype;

import javafx.scene.paint.Color;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.RenderListener;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.effects.AbstractParticle;

/**
 *
 * @author rpetit
 */
public class CircleParticle extends AbstractParticle implements RenderListener {

    private final Vect2 screenSize;
    private double size = 6;
    private Color color = Color.WHITE;

    public CircleParticle(final Vect2 screenSize) {
        this.screenSize = screenSize.copy();
        super.addRenderListener(this);
    }

    @Override
    public void onRender(final Graphics gc) {
        gc.setFill(this.color);
        gc.fillOval(position().x().asDouble(), position().y().asDouble(), this.size, this.size);
    }

    @Override
    public boolean beforeRender(final Graphics gc) {
        return true;
    }

    public CircleParticle size(final double size) {
        this.size = size;
        return this;
    }

    public double size() {
        return this.size;
    }

    public CircleParticle color(final Color color) {
        this.color = color;
        return this;
    }

    public boolean isAlive() {
        return isVisible() && position().x().positive() && position().x().lowerThan(this.screenSize.x()) && position().y().positive() && position().y().lowerThan(screenSize.y());
    }
}
