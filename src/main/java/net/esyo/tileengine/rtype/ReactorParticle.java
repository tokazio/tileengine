package net.esyo.tileengine.rtype;

import javafx.scene.paint.Color;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.effects.*;

/**
 *
 * @author rpetit
 */
public class ReactorParticle extends AbstractLivingParticle {

    private double size = 10;
    private Color color = Color.WHITE;
    private Color colorV = Color.WHITE;
    private double opacityV = 1;
    private double sizeV = 1;
    private long decay = 0;

    public ReactorParticle() {
        super();
    }

    @Override
    public void render(final Graphics gc) {
        if (isAlive()) {
            gc.setFill(colorV, opacityV);
            double s = size * sizeV;
            //todo fillCircle
            gc.fillOval(position().x().sub(s).asDouble(), position().y().sub(s).asDouble(), s, s);
        }
    }


    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        if (velocity().y().zero()) {
            acceleration (Vect2.zero());
        }
        super.update(screenSize, elapsedTime);
        if (isAlive()) {
            if (System.currentTimeMillis() - birth > decay) {
                double p = timeToLive / (lifeTime - decay);
                sizeV = p;
                opacityV = p;
                colorV = Color.hsb(color.getHue(), 1 - p, color.getBrightness());
            }
        }
    }


    public ReactorParticle color(final Color color) {
        this.color = color;
        return this;
    }

    public ReactorParticle size(final double d) {
        this.size = d;
        return this;
    }

    public ReactorParticle decay(final long l) {
        this.decay = l;
        return this;
    }


    @Override
    public ReactorParticle rebirth() {
        super.rebirth();
        size = 10;
        opacityV = 1;
        sizeV = 1;
        decay = 0;
        return this;
    }

}
