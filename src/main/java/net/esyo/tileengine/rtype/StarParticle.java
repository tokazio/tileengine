package net.esyo.tileengine.rtype;

import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.effects.AbstractParticle;

/**
 *
 * @author rpetit
 */
public class StarParticle extends AbstractParticle {

    private static final double SPEED = 4;//upper is slower

    private double size = 2;
    private double opacity = 1;

    public StarParticle(final Vect2 screenSize, boolean start) {
        super();
        this.size = Math.random() * 3 + 1;
        super.velocity(Vect2.of(((Math.random() + 1) * -size) / SPEED, 0));
        this.opacity = (((Math.random() * size) / size) + 0.1) % 1;
        super.position(Vect2.of(start ? screenSize.x().random() : screenSize.x(), screenSize.y().random()));
    }

    @Override
    public void render(Graphics gc) {
        gc.setFill(1, this.opacity);
        gc.fillOval(position().x().asDouble(), position().y().asDouble(), this.size, this.size);
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        super.update(screenSize, elapsedTime);
        this.visible(position().x().positive() && position().y().positive() && position().y().lowerThan(screenSize.y()));
    }

}
