package net.esyo.tileengine.rtype;

/**
 *
 * @author rpetit
 */
public interface Producer {

    void produce(SpaceItem collectable);

}
