package net.esyo.tileengine.rtype;

import javafx.scene.paint.Color;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Positionable;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.effects.*;

/**
 *
 * @author rpetit
 */
public class Reactor extends AbstractLivingParticleEmitter<ReactorParticle> {

    private boolean on = true;
    private final Color c = Color.hsb(200, 1.0, 1.0);

    public Reactor(final int number) {
        super(number);
    }

    public Reactor(final Vect2 position, final int number) {
        super(position, number);
    }

    @Override
    public ReactorParticle resetParticle(final ReactorParticle p) {
        double y = (Math.random() * 2 - 1);
        double v = 0.01;
        if (y < 0) {
            v = -v;
        }
        p.position(Vect2.of(position.x(), position.y().add(y)));
        p.timeToLive(200)
                .velocity(Vect2.of((Math.random() * -1), v))
                .acceleration(Vect2.of(0, -v / 10));
        p.color(c).size(3);
        return p;
    }

    @Override
    public ReactorParticle createParticle() {
        return new ReactorParticle();
    }

    @Override
    public <T extends Positionable> T position(final Vect2 point) {
        this.position = point.copy();
        return (T) this;
    }

    @Override
    public void render(final Graphics gc) {
        if (this.on) {
            super.render(gc);
        }
    }

    public void start() {
        this.on = true;
    }

    public void stop() {
        this.on = false;
    }
}
