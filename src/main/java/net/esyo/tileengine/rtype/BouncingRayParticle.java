package net.esyo.tileengine.rtype;

import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.effects.AbstractParticle;
import static net.esyo.tileengine.engine.core.utils.AngleUtils.A360;

/**
 *
 * @author romainpetit
 */
public class BouncingRayParticle extends AbstractParticle {

    private double angle;//radian
    private double size = 100;
    private double sizeV = 0;

    private Vect2 bounce;
    private BouncingRayParticle next;

    /**
     *
     * @param angle radian
     * @param life
     */
    public BouncingRayParticle(final double angle, final double life) {
        super();
        this.angle = angle;
        this.size = life * size;
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        if (sizeV < size) {
            sizeV += velocity().x().asDouble();
        }
        if (isBouncing() && position().distance(this.bounce).asDouble() <= 0.5) {
            visible(false);
        }
        if (sizeV >= size) {
            this.position().add(x(this.velocity().x().asDouble()), y(this.velocity().y().asDouble()));
        }
        this.velocity().add(this.acceleration());
    }

    private double x(double v) {
        return Math.cos(angle) * v;
    }

    private double y(double v) {
        return Math.sin(angle) * -v;
    }

    @Override
    public void render(final Graphics gc) {
        if (isVisible()) {
            gc.setLineWidth(3);
            gc.setStroke(0,144,255,128);
            gc.strokeLine(position(), end());
            if (isBouncing()) {
                gc.setFill(0,144,255,128);
                gc.fillCircle(bounce, 10);
            }
    
            gc.setLineWidth(1);
            gc.setStroke(1,0.6);
            gc.strokeLine(position(), end());
            if (isBouncing()) {
                gc.setFill(1,0.6);
                gc.fillCircle(bounce, 4);
            }

        }
    }

    public BouncingRayParticle bouncing(final Vect2 point, final BouncingRayParticle next) {
        if (bounce == null) {
            this.next = next;
            this.bounce = point.copy();
        }
        return this;
    }

    public boolean isBouncing() {
        return this.bounce != null;
    }

    public boolean hasNext() {
        return this.next != null;
    }

    public Vect2 end() {
        return isBouncing() ? this.bounce : Vect2.of(position().x().asDouble() + x(sizeV), position().y().asDouble() + y(sizeV));
    }

    /**
     *
     * @return angle radian
     */
    public double angle() {
        return this.angle;
    }

    /**
     *
     * @param angle radian
     * @return
     */
    public BouncingRayParticle angle(double angle) {
        this.angle = angle % A360;
        return this;
    }

    public Vect2 asVector() {
        final Vect2 v = end().copy().sub(position());
        v.y(v.y().inverse());
        return v;
    }

}
