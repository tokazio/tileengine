package net.esyo.tileengine.shootem;

import java.util.ArrayList;
import java.util.List;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Positionable;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.effects.*;

/**
 *
 * @author rpetit
 */
public class ExplodeParticleEmitter implements LivingParticleEmitter<ExplodeParticle> {

    protected final List<ExplodeParticle> particles = new ArrayList<>();
    protected final int number;
    protected Vect2 position;
    protected long lastCreated;

    public ExplodeParticleEmitter(int number) {
        this.number = number;
    }

    public void explode() {
        for (int i = 0; i < number; i++) {
            this.particles.add(createParticle());
        }
    }

    @Override
    public <T extends Positionable> T position(Vect2 point) {
        this.position = point;
        return (T) this;
    }

    @Override
    public void render(final Graphics gc) {
        this.particles.forEach(particle -> particle.render(gc));
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        for (ExplodeParticle particle : this.particles) {
            if (particle.isAlive()) {
                particle.update(screenSize, elapsedTime);
            }
        }
    }

    @Override
    public ExplodeParticle resetParticle(ExplodeParticle p) {
        return p;
    }

    @Override
    public ExplodeParticle createParticle() {
        //circular random------
        double a = Math.random()*2*Math.PI;
        double w = Math.random()*10-5;
        double x = Math.cos(a) * w;
        double y = Math.sin(a) * w;
        //---------------------
        final ExplodeParticle p = new ExplodeParticle()
                .size(Math.random() * 5)
                .timeToLive(1000)
                .velocity(Vect2.of(x, y))
                .acceleration(Vect2.of(x / 100, y / 100));
        return p.position(this.position.copy());
    }

    @Override
    public Vect2 position() {
        return this.position;
    }

}
