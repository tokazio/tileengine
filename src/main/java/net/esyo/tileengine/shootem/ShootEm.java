package net.esyo.tileengine.shootem;

import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import static javafx.scene.input.KeyCode.E;
import static javafx.scene.input.KeyCode.SPACE;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javax.xml.bind.JAXBException;
import net.esyo.tileengine.engine.core.Engine;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.OnKey;
import net.esyo.tileengine.engine.core.OnMouse;
import net.esyo.tileengine.engine.core.TileSetException;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.map.tmx.MapException;

/**
 *
 * @author rpetit
 */
public class ShootEm extends Application implements OnMouse, OnKey {

    private final Vect2 screenSize = Vect2.of(1024, 768);
    private final Vect2 mousePosition = Vect2.zero();

    private final SpaceCraft craft = new SpaceCraft();

    @Override
    public void start(Stage theStage) throws FileNotFoundException, IOException, TileSetException, JAXBException, MapException {

        //2D engine to render the tiles/sprites
        new Engine(theStage, screenSize) {

            @Override
            public void initialize(Graphics gc) {
                craft.position(screenSize.copy().div(2));
                this.addMousable(ShootEm.this);
                this.addKeyable(ShootEm.this);
            }

            @Override
            public void update(Graphics gc, long elapsedTime) {
                gc.setFill(0,0.7);//alpha making a sort of fading when erasing the last frame
                gc.fillRect(screenSize);
                //updating
                craft.move(mousePosition);
                craft.update(gc.screenSize(), elapsedTime);
                //rendering
                craft.render(gc);
                //
            }
        }.start();
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void onMouseMoved(MouseEvent e) {
        mousePosition.to(e.getSceneX(), e.getSceneY());
    }

    @Override
    public void onKeyPress(KeyEvent k) {
        System.out.println(k);
        if (k.getCode() == SPACE) {
            craft.fire();
        }
        if (k.getCode() == E) {
            craft.explode();
        }
    }

    @Override
    public void onKeyUp(KeyEvent k) {

    }

}
