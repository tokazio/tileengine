package net.esyo.tileengine.shootem;

import javafx.scene.paint.Color;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.effects.*;

/**
 *
 * @author rpetit
 */
public class ExplodeParticle extends AbstractLivingParticle {

    double size = 10;
    Color color = Color.WHITE;
    double opacityV = 1;
    double sizeV = 1;

    public ExplodeParticle() {
        super();
    }

    @Override
    public void render(Graphics gc) {
        if (isAlive()) {
            gc.setFill(color, opacityV);
            double s = size * sizeV;
            //todo fillCircle
            gc.fillOval(position().x().sub(s).asDouble(), position().y().sub(s).asDouble(), s, s);
        }
    }


    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        super.update(screenSize, elapsedTime);
        if (isAlive()) {
            opacityV = timeToLive / lifeTime;
            sizeV = timeToLive / lifeTime;
        }
    }


    public ExplodeParticle color(Color color) {
        this.color = color;
        return this;
    }

    public ExplodeParticle size(double d) {
        this.size = d;
        return this;
    }

    @Override
    public ExplodeParticle rebirth() {
        super.rebirth();
        size = 10;
        color = Color.WHITE;
        opacityV = 1;
        sizeV = 1;
        return this;
    }

}
