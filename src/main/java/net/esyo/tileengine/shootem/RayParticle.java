package net.esyo.tileengine.shootem;

import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.effects.AbstractParticle;

/**
 *
 * @author rpetit
 */
public class RayParticle extends AbstractParticle {

    private double size = 10;
    private double angle;//radian

    @Override
    public void render(Graphics gc) {
        gc.setStroke(1);
        gc.strokeLine(position(), Vect2.fromRadian(angle).mul(size).add(position()));
    }

    @Override
    public void update(final Vect2 screenSize, long elapsedTime) {
        this.position().add(Vect2.fromRadian(angle).mul(velocity()));
        this.velocity().add(this.acceleration());
    }

    public RayParticle size(double size) {
        this.size = size;
        return this;
    }

    /**
     *
     * @param angle radian
     * @return
     */
    public RayParticle angle(double angle) {
        this.angle = angle;
        return this;
    }
}
