package net.esyo.tileengine.shootem;

import java.util.ArrayList;
import java.util.List;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Positionable;
import net.esyo.tileengine.engine.core.Pt;
import net.esyo.tileengine.engine.core.Renderable;
import net.esyo.tileengine.engine.core.Updatable;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author rpetit
 */
public class SpaceCraft implements Positionable, Renderable, Updatable {

    private Vect2 position = Vect2.zero();
    private Pt angle;//radian

    private final List<RayParticle> rays = new ArrayList<>();
    private final ExplodeParticleEmitter emitter = new ExplodeParticleEmitter(200);
    private boolean exploded;

    @Override
    public void render(Graphics gc) {
        if (!exploded) {
            gc.save();
            gc.rotate(angle.copy().degrees().add(90).asDouble(), position.x().asDouble(), position.y().asDouble());//tlpx + image.getWidth() / 2, tlpy + image.getHeight() / 2);
            gc.setStroke(1);
            gc.strokeRect(position.x().sub(20).asDouble(), position.y().sub(20).asDouble(), 40, 40);
            gc.strokeLine(position.x().asDouble(), position.y().sub(20).asDouble(), position.x().asDouble(), position.y().asDouble());
            gc.restore();
        }
        rays.forEach(r -> r.render(gc));
        emitter.render(gc);
    }

    @Override
    public <T extends Positionable> T position(Vect2 point) {
        this.position = point;
        this.emitter.position(point);
        return (T) this;
    }

    @Override
    public Vect2 position() {
        return position;
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        rays.forEach(r -> r.update(screenSize, elapsedTime));
        emitter.update(screenSize, elapsedTime);
    }

    public void move(Vect2 mouse) {
        final Vect2 delta = mouse.copy().sub(position);
        angle = Pt.of(-Math.atan2(delta.x().asDouble(), delta.y().asDouble()) - Math.PI / 2);
    }

    public void fire() {
        System.out.println("fire");
        final RayParticle ray = new RayParticle()
                .angle(angle.asDouble())
                .velocity(Vect2.of(2, 2))
                .acceleration(Vect2.of(0.1, 0.1));
        rays.add(ray.position(position.copy()));
    }

    public void explode() {
        System.out.println("explode");
        exploded = true;
        emitter.explode();
    }

}
