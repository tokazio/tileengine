package net.esyo.tileengine.engine;

import java.util.HashSet;

/**
 *
 * @author rpetit
 * @param <T>
 */
public class ObjectSet<T>
        extends HashSet<T> {

    //Hide constructor
    private ObjectSet() {
        super();
    }

    public static <T> ObjectSet<T> noneOf() {
        return new ObjectSet<>();
    }

    @SafeVarargs
        public static <T> ObjectSet<T> of(final T... values) {
        ObjectSet<T> s = noneOf();
        if (values != null) {
            for (T i : values) {
                s.add(i);
            }
        }
        return s;
    }
}
