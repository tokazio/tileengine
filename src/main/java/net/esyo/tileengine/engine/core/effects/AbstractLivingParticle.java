package net.esyo.tileengine.engine.core.effects;

import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author rpetit
 */
public abstract class AbstractLivingParticle extends AbstractParticle implements LivingParticle {

    protected long timeToLive = 0;//in ms
    protected double lifeTime = 0;//in ms total life time to live
    protected long birth = System.currentTimeMillis();

    protected AbstractLivingParticle() {
        super();
        rebirth();
    }

    @Override
    public boolean isAlive() {
        return this.timeToLive > 0;
    }

    @Override
    public <T extends Particle> T timeToLive(long ms) {
        this.timeToLive = ms;
        this.lifeTime = ms;
        this.birth = System.currentTimeMillis();
        return (T) this;
    }

    @Override
    public long timeToLive() {
        return this.timeToLive;
    }

    @Override
    public long lifeTime() {
        return this.timeToLive;
    }

    @Override
    public long birth() {
        return this.birth;
    }

    @Override
    public LivingParticle rebirth() {
        this.position(Vect2.zero());
        this.velocity(Vect2.zero());
        this.acceleration(Vect2.zero());
        this.timeToLive = 0;
        this.lifeTime = 0;
        this.birth = System.currentTimeMillis();
        return this;
    }

    @Override
    public void update(final Vect2 screenSize, long elapsedTime) {
        if (isAlive()) {
            this.timeToLive -= elapsedTime;
            super.update(screenSize, elapsedTime);
        }
    }
}
