package net.esyo.tileengine.engine.core;

import java.util.HashMap;
import java.util.Map;

/**
 * Sprite playing multiple key frame from a tileset.
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class AnimatedSprite extends Sprite implements Animable {

    private static final boolean DEBUG = false;
    private boolean playing;
    private boolean looping;
    private int playAtLeast = 0;
    protected int duration = 128;
    private long previousTime;
    protected int frame = 0;
    protected int loopFrame = 0;
    private final Map<String, KeyFrames> keyFrames = new HashMap<>();
    private String currentKeyFramesName = null;
    private int pause;

    public AnimatedSprite(final TileSet tile) {
	super(tile, 0);
    }

    public AnimatedSprite(final TileSet tiles, final Vect2 position) {
        super(tiles, 0, position);
    }

    @Override
    public <T extends Animable> T play(final boolean looping) {
	if (!isPlaying()) {
	    this.looping = looping;
	    this.previousTime = System.currentTimeMillis();
	    this.playing = true;
	}
	return (T) this;
    }

    @Override
    public <T extends Animable> T resume() {
	return play(looping);
    }

    @Override
    public <T extends Animable> T pause() {
	if (isPlaying() && frame >= playAtLeast) {
	    this.playing = false;
	}
	return (T) this;
    }

    @Override
    public <T extends Animable> T stop() {
	if (isPlaying() && frame >= playAtLeast) {
	    this.playing = false;
	    this.frame = 0;
	}
	return (T) this;
    }

    public <T extends AnimatedSprite> T addKeyFrames(final String name, final KeyFrames animation) {
	this.addKeyFrames(name, animation, true);
	return (T) this;
    }

    public <T extends AnimatedSprite> T addKeyFrames(final String name, final KeyFrames animation, boolean set) {
	this.keyFrames.put(name, animation);
	if (set) {
	    try {
		this.selectKeyFrames(name);
	    } catch (AnimatedSpriteException ex) {
		//We add it just before, then no AnimatedSpriteException possible here!
	    }
	}
	return (T) this;
    }

    public KeyFrames keyFrames(final String name) {
	return this.keyFrames.get(currentKeyFramesName);
    }

    public KeyFrames selectedKeyFrames() {
	return this.keyFrames(this.currentKeyFramesName);
    }

    public String selectedKeyFramesName() {
	return this.currentKeyFramesName;
    }

    public <T extends AnimatedSprite> T selectKeyFrames(final String name) throws AnimatedSpriteException {
	if (!this.keyFrames.containsKey(name)) {
	    throw new AnimatedSpriteException("Animation '" + name + "' doesn't exists. Choose in: " + this.keyFrames.keySet());
	}
	this.currentKeyFramesName = name;
	return (T) this;
    }

    @Override
    public <T extends Animable> T onEnd() {
	//@todo: call onEnd
	return (T) this;
    }

    public <T extends AnimatedSprite> T playAtLeast(int i) {
	playAtLeast = i;
	return (T) this;
    }

    protected int getFrame() {
	if (!this.keyFrames.isEmpty() && this.selectedKeyFramesName() != null) {
            if (isPlaying()) {
                if (this.frame == 0 && pause > 0 && System.currentTimeMillis() - this.previousTime < this.pause) {
                    //wait
                } else {
                    if (System.currentTimeMillis() - this.previousTime > this.duration) {
		    this.frame++;
                    if (this.frame > this.selectedKeyFrames().count() - 1) {
			if (this.looping) {
			    this.frame = loopFrame;
			} else {
			    this.playing = false;
			    this.frame--;
			}
		    }
		    this.previousTime = System.currentTimeMillis();
                    }
                }
	    } else {
		this.frame = 0;
	    }
            return this.selectedKeyFrames().getKey(this.frame);
	} else {
	    return this.defaultTileCode();
	}
    }

    @Override
    public void render(final Graphics gc) {
        this.tileSet().render(gc, this.getFrame(), this.position());
	if (DEBUG) {
            gc.strokeText(frame + "", this.position().x().asDouble(), this.position().y().add(20).asDouble());
	}
    }

    @Override
    public boolean isPlaying() {
	return this.playing;
    }

    public boolean isLopped() {
	return this.looping;
    }

    public <T extends AnimatedSprite> T pause(final int pause) {
        this.pause = pause;
        return (T) this;
    }

    public <T extends AnimatedSprite> T duration(final int duration) {
	this.duration = duration;
	return (T) this;
    }

    public <T extends AnimatedSprite> T loopFrame(final int frame) {
	this.loopFrame = frame;
	return (T) this;
    }
}
