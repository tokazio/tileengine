package net.esyo.tileengine.engine.core;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
@FunctionalInterface
public interface OnRenderEvent {

    void call(final Graphics gc);
}
