package net.esyo.tileengine.engine.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * List of frame #id for animate a sprite from a tileset.
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class KeyFrames {

    private final List<Integer> keys = new ArrayList<>();

    public KeyFrames(final Integer... newKeys) {
	this.add(newKeys);
    }

    public KeyFrames add(final Integer newKey) {
	this.keys.add(newKey);
	return this;
    }

    public final KeyFrames add(final Integer... newKeys) {
	this.keys.addAll(Arrays.asList(newKeys));
	return this;
    }

    public int getKey(final int id) {
	return this.keys.get(id);
    }

    public int count() {
	return this.keys.size();
    }

    public KeyFrames reverse() {
        Collections.reverse(this.keys);
        return this;
    }

    public KeyFrames clear() {
	this.keys.clear();
	return this;
    }
}
