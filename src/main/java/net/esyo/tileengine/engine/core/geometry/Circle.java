package net.esyo.tileengine.engine.core.geometry;

import net.esyo.tileengine.engine.core.Pt;
import net.esyo.tileengine.engine.core.Vect2;

public class Circle {

    private Vect2 center;
    private double radius;

    public Circle(final Vect2 position, final double r) {
        this.center = position;
        this.radius = r;
    }

    public void center(final Vect2 position) {
        this.center = position;
    }

    public void radius(final double r) {
        this.radius = r;
    }

    public Pt x() {
        return this.center.x();
    }

    public Pt y() {
        return this.center.y();
    }

    public double radius() {
        return this.radius;
    }

    public boolean containsPoint(final Vect2 point) {
        return this.center.distance(point).lowerThan(this.radius);
    }
}
