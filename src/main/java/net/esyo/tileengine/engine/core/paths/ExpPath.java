package net.esyo.tileengine.engine.core.paths;

/**
 *
 * @author rpetit
 */
public class ExpPath implements Path {

    @Override
    public double y(double x) {
        return Math.exp(x);
    }
}
