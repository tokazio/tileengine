package net.esyo.tileengine.engine.core.effects;

import javafx.scene.paint.Color;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author rpetit
 */
public class CircleParticle extends AbstractLivingParticle {

    public double size = 10;
    public Color color = Color.WHITE;
    double startOpacity = 1;
    double opacityV = 1;
    double sizeV = 1;
    long decay = 0;
    private boolean visible = true;

    public CircleParticle() {
        super();
    }

    @Override
    public void visible(boolean b){
        this.visible = b;
    }

    @Override
    public boolean isVisible(){
        return this.visible;
    }

    @Override
    public void render(Graphics gc) {
        if (isAlive()) {
            gc.setFill(color, opacityV * startOpacity);
            double s = size * sizeV;
            //todo fillCircle
            gc.fillOval(position().x().sub(s).asDouble(), position().y().sub(s).asDouble(), s, s);
        }
    }


    @Override
    public void update(final Vect2 screenSize, long elapsedTime) {
        super.update(screenSize, elapsedTime);
        if (isAlive()) {
            if (System.currentTimeMillis() - birth > decay) {
                opacityV = timeToLive / (lifeTime - decay);
                sizeV = timeToLive / (lifeTime - decay);
            }
        }
    }


    public CircleParticle color(Color color) {
        this.color = color;
        return this;
    }

    public CircleParticle size(double d) {
        this.size = d;
        return this;
    }

    public CircleParticle decay(long l) {
        this.decay = l;
        return this;
    }

    public CircleParticle opacity(double opacity) {
        this.startOpacity = opacity;
        return this;
    }

    @Override
    public CircleParticle rebirth() {
        super.rebirth();
        size = 10;
        color = Color.WHITE;
        startOpacity = 1;
        opacityV = 1;
        sizeV = 1;
        decay = 0;
        return this;
    }

}
