package net.esyo.tileengine.engine.core.effects;

/**
 *
 * @author rpetit
 */
public interface LivingParticle extends Particle {

    boolean isAlive();

    <T extends Particle> T timeToLive(final long ms);

    long timeToLive();

    long lifeTime();

    long birth();

    <T extends Particle> T rebirth();
}
