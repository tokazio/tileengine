package net.esyo.tileengine.engine.core;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class AnimatedSpriteException extends RuntimeException {

    public AnimatedSpriteException(final String s) {
	super(s);
    }

}
