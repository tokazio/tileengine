package net.esyo.tileengine.engine.core.effects;

import java.util.ArrayList;
import java.util.List;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Movable;
import net.esyo.tileengine.engine.core.Positionable;
import net.esyo.tileengine.engine.core.RenderListener;
import net.esyo.tileengine.engine.core.UpdateListener;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author rpetit
 */
public abstract class AbstractParticle implements Particle {

    private Vect2 position = Vect2.zero();
    private Vect2 velocity = Vect2.zero();
    private Vect2 acceleration = Vect2.zero();
    private boolean visible = true;
    private final List<RenderListener> renderListeners = new ArrayList<>();
    private final List<UpdateListener> updateListeners = new ArrayList<>();

    /**
     * the position will be changed by the velocity vector at each loop (the
     * velocity will be changed itself by the acceleration at each loop too) you
     * can override the <code>applyForce</code> method to change these values
     * too (adding gravitiy, wind or other...)
     *
     * @param <T>
     * @param point a copy of this vector will be stored as the position
     * @return
     */
    @Override
    public <T extends Positionable> T position(final Vect2 point) {
        this.position = point.copy();
        return (T) this;
    }

    /**
     * the velocity will be added to the position at each loop you can override
     * the <code>applyForce</code> method to change these values too (adding
     * gravitiy, wind or other...)
     *
     * @param <T>
     * @param point a copy of this vector will be stored as the velocity
     * @return me
     */
    @Override
    public <T extends Movable> T velocity(final Vect2 point) {
        this.velocity = point.copy();
        return (T) this;
    }

    /**
     * the acceleration will be added to the velocity at each loop you can
     * override the <code>applyForce</code> method to change these values too
     * (adding gravitiy, wind or other...)
     *
     * @param <T>
     * @param point a copy of this vector will be stored as the acceleration
     * @return me
     */
    @Override
    public <T extends Movable> T acceleration(final Vect2 point) {
        this.acceleration = point.copy();
        return (T) this;
    }

    /**
     * be carefull, position (left,top) is mutable, use <code>.copy()</code> the
     * position will be changed by the velocity vector at each loop (the
     * velocity will be changed itself by the acceleration at each loop too) you
     * can override the <code>applyForce</code> method to change these values
     * too (adding gravitiy, wind or other...)
     *
     * @return left,top position
     */
    @Override
    public Vect2 position() {
        return this.position;
    }

    /**
     * be carefull, acceleration (x,y) is mutable, use <code>.copy()</code> the
     * acceleration will be added to the velocity at each loop
     *
     * @return the acceleration vector
     */
    @Override
    public Vect2 acceleration() {
        return this.acceleration;
    }

    /**
     * be carefull, velocity (x,y) is mutable, use <code>.copy()</code> the
     * velocity will be added to the position at each loop
     *
     * @return the velocity vector
     */
    @Override
    public Vect2 velocity() {
        return this.velocity;
    }

    /**
     * you can override the <code>applyForce</code> method to change position
     * (adding gravitiy, wind or other...)
     *
     * @param <T> a new vector applied to the position at each loop
     * @param velocity
     * @return
     */
    @Override
    public <T extends Particle> T applyForce(final Vect2 velocity) {
        return (T) this;
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        boolean canUpdate = true;
        for (UpdateListener l : this.updateListeners) {
            canUpdate = canUpdate && l.beforeUpdate(elapsedTime);
        }
        if (canUpdate) {
            this.position.add(this.velocity);
            this.velocity.add(this.acceleration);
            this.updateListeners.forEach(l -> l.afterUpdate(elapsedTime));
        }
    }

    @Override
    public void render(final Graphics gc) {
        boolean canRender = true;
        for (RenderListener l : this.renderListeners) {
            canRender = canRender && l.beforeRender(gc);
        }
        if (canRender && isVisible()) {
            this.renderListeners.forEach(e -> e.onRender(gc));
        }
    }

    public void addRenderListener(final RenderListener renderListener) {
        this.renderListeners.add(renderListener);
    }

    public void removeRenderListener(final RenderListener renderListener) {
        this.renderListeners.remove(renderListener);
    }

    public void clearRenderListener() {
        this.renderListeners.clear();
    }

    public void addUpdateListener(final UpdateListener updateListener) {
        this.updateListeners.add(updateListener);
    }

    public void removeUpdateListener(final UpdateListener updateListener) {
        this.updateListeners.remove(updateListener);
    }

    public void clearUpdateListener() {
        this.updateListeners.clear();
    }

    public void visible(boolean b) {
        this.visible = b;
    }

    public boolean isVisible() {
        return this.visible;
    }
}
