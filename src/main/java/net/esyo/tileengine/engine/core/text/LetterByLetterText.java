package net.esyo.tileengine.engine.core.text;

import com.sun.javafx.tk.FontMetrics;
import com.sun.javafx.tk.Toolkit;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.OnKey;
import net.esyo.tileengine.engine.core.Positionable;
import net.esyo.tileengine.engine.core.Renderable;
import net.esyo.tileengine.engine.core.Updatable;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class LetterByLetterText implements Positionable, Renderable, Updatable, OnKey {

    private static double letterWidth = 10;
    private static double letterHeight = 20;
    private int fastDuration = 0;
    private int duration = 112;
    private boolean fast = false;
    private final String text;
    private int col;
    private int line;
    private boolean playing = false;
    private Vect2 position;
    private long previousTime;
    private final List<String> lines = new ArrayList<>();

    public LetterByLetterText(final String text, final Vect2 position, final Vect2 size) {
	this.text = text;
        final String[] ns = text.split("\n");
        final int maxl = size.x().div(LetterByLetterText.letterWidth).floor().asInt();
	for (String s : ns) {
	    int i = 0;
	    while (i < s.length()) {
		int m = maxl;
		if (m + i > s.length()) {
		    m = s.length() - i;
		}
		lines.add(s.substring(i, m + i).trim());
		i += m;
	    }
	}
	this.position = position;
    }

    @Override
    public void render(final Graphics gc) {
	gc.setFill(Color.WHITE);
        double y = this.position.y().add(LetterByLetterText.letterHeight).asDouble();
	if (this.line >= 1) {
	    for (int i = 0; i < this.line; i++) {
                gc.fillText(this.lines.get(i), position.x().asDouble(), y);
		y += LetterByLetterText.letterHeight;
	    }
	}
	if (line < lines.size() && col < text.length()) {
            gc.fillText(this.lines.get(this.line).substring(0, this.col), this.position.x().asDouble(), y);
	}
    }

    @Override
    public <T extends Positionable> T position(final Vect2 point) {
	this.position = point;
	return (T) this;
    }

    @Override
    public Vect2 position() {
        return this.position;
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
	if (this.isPlaying()) {
	    if (System.currentTimeMillis() - this.previousTime > (this.fast ? this.fastDuration : this.duration)) {
		this.col++;
		if (this.col > this.lines.get(line).length()) {
		    this.col = 0;
		    line++;
		    if (line > lines.size() - 1) {
//			waitUser();
			stop();
		    }
		}
		this.previousTime = System.currentTimeMillis();
	    }
	}
    }

    public LetterByLetterText stop() {
	this.playing = false;
	return this;
    }

    public LetterByLetterText play() {
	this.line = 0;
	this.col = 0;
	this.playing = true;
	this.previousTime = System.currentTimeMillis();
	return this;
    }

    public boolean isPlaying() {
	return this.playing;
    }

    public void waitUser() {
	System.out.println("Plus de place, attend...");
    }

    public static void getTextSize(final Graphics gc) {
	final FontMetrics fm = Toolkit.getToolkit().getFontLoader().getFontMetrics(gc.getFont());
	letterWidth = fm.computeStringWidth("T");
	letterHeight = fm.getLineHeight();
    }

    @Override
    public void onKeyPress(final KeyEvent k) {
	fast = true;
    }

    @Override
    public void onKeyUp(final KeyEvent k) {
	fast = false;
    }

}
