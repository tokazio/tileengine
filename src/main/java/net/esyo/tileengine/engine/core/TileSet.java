package net.esyo.tileengine.engine.core;

import java.io.File;
import java.io.FileNotFoundException;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import net.esyo.tileengine.engine.core.utils.ImageUtils;

/**
 * Tileset image accessible by each tile index from top,left to bottom right (row by row).
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class TileSet {

    private Image tilesetImage;
    private final Vect2 tileSize;
    private final Vect2 nb;

    public TileSet(final String filename, final Vect2 tileSize) throws FileNotFoundException, TileSetException {
        if (tileSize.x().lowerOrEquals(0) || tileSize.y().lowerOrEquals(0)) {
	    throw new TileSetException("Tile width or height can't be zero or negative");
	}
        this.tileSize = tileSize.copy();
        final File f = new File(filename);
	if (!f.exists()) {
	    throw new FileNotFoundException("TileSet file '" + filename + "' doesn't exists!");
	} else {
	    tilesetImage = new Image("File:" + filename);
            if (tilesetImage.getWidth() < tileSize().x().asDouble() || tilesetImage.getHeight() < tileSize().y().asDouble()) {
                throw new TileSetException("The tile set image (" + tilesetImage.getWidth() + "x" + tilesetImage.getHeight() + "pxs) is smaller than a tile (" + tileSize().x() + "x" + tileSize().y() + "pxs)");
            }
            this.nb = Vect2.of(this.tilesetImage.getWidth() / tileSize().x().asDouble(), this.tilesetImage.getHeight() / tileSize().y().asDouble());
	}
    }

    public TileSet(final Image image, final Vect2 tileSize) throws TileSetException {
        if (tileSize.x().lowerOrEquals(0) || tileSize.y().lowerOrEquals(0)) {
            throw new TileSetException("Tile width or height can't be zero or negative");
        }
        this.tileSize = tileSize.copy();
        tilesetImage = image;
        if (tilesetImage.getWidth() < tileSize().x().asDouble() || tilesetImage.getHeight() < tileSize().y().asDouble()) {
            throw new TileSetException("The tile set image (" + tilesetImage.getWidth() + "x" + tilesetImage.getHeight() + "pxs) is smaller than a tile (" + tileSize().x() + "x" + tileSize().y() + "pxs)");
        }
        this.nb = Vect2.of(this.tilesetImage.getWidth() / tileSize().x().asDouble(), this.tilesetImage.getHeight() / tileSize().y().asDouble());
    }

    /**
     * Set transparent color from a point in the tile set.
     *
     * Detect color at 'x,y' and rewrite image in memory with this color to transparent
     * Use setTransparent(Color) after the detection
     *
     * @param point coordinates x,y of the color to convert to transparent
     *
     * @return chaining
     */
    public TileSet transparent(final Vect2 point) {
        return transparent(ImageUtils.pickColor(tilesetImage, point));
    }

    /**
     * Set the color 'col' to transparent.
     *
     * Rewrite image in memory with this color to transparent
     *
     * @param col Color to set to transparent
     *
     * @return chaining
     */
    public TileSet transparent(final Color col) {
	tilesetImage = ImageUtils.setTransparentColor(tilesetImage, col);
	return this;
    }

    /**
     * Render the tile by its 'code' on the 'gc' at position 'x','y'.
     * If 'code' is not between 0 and the max tile number, no render is processed
     *
     * @param gc   GraphicsContext to draw on
     * @param code tile code to draw
     * @param position gc x,y coordinate for where to draw the tile
     */
    public void render(final Graphics gc, final int code, final Vect2 position) {
        if (code >= 0 && code < this.numberOfTiles()) {
            final Vect2 p = Vect2.of(this.tileSize().x().mul((code % this.numberOfColumns())), this.tileSize().y().mul((code / this.numberOfColumns())));
            final Tile t = new Tile(p, tileSize());
            render(gc, t, position);
	}
    }

    public void render(final Graphics gc, final Tile tile, final Vect2 position) {
        gc.drawImage(image(), tile.position().x().asDouble(), tile.position().y().asDouble(), tile.size().x().asDouble(), tile.size().y().asDouble(), position.x().asDouble(), position.y().asDouble(), tile.size().x().asDouble(), tile.size().y().asDouble());
    }

    public final Vect2 tileSize() {
        return this.tileSize;
    }

    public int numberOfTiles() {
        return numberOfColumns() * numberOfRows();
    }

    public int numberOfColumns() {
        return this.nb.x().asInt();
    }

    public int numberOfRows() {
        return this.nb.y().asInt();
    }

    public Image image() {
	return this.tilesetImage;
    }

    /**
     * be carefull, size is mutable, use <code>.copy()</code>
     * @return width, height size
     */
    public Vect2 size(){
        return this.tileSize;
    }

    /**
     * Debug purpose.
     * Render the tile set to 'gc' with code on each tile.
     *
     * @param gc GraphicsContext to draw on
     */
    public void show(final Graphics gc) {
	int k = 0;
	gc.setFill(Color.WHITE);
	gc.setStroke(Color.BLACK);
        for (int y = 0; y < nb.y().asInt(); y++) {
            for (int x = 0; x < nb.x().asInt(); x++) {
                render(gc, k, tileSize.mul(Vect2.of(x, y)));
                gc.strokeText(k + "", tileSize.x().mul(x).add(4).asDouble(), tileSize.y().mul(y).add(16).asDouble());
                gc.fillText(k + "", tileSize.x().mul(x).add(4).asDouble(), tileSize.y().mul(y).add(16).asDouble());
                gc.strokeRect(tileSize.x().mul(x).asDouble(), tileSize.y().mul(y).asDouble(), tileSize.x().asDouble(), tileSize.y().asDouble());
		k++;
	    }
	}
    }
}
