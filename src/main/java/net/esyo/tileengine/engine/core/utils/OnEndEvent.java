package net.esyo.tileengine.engine.core.utils;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
@FunctionalInterface
public interface OnEndEvent {
    
    void call();
}
