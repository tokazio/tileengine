package net.esyo.tileengine.engine.core;

import static net.esyo.tileengine.engine.core.utils.AngleUtils.A0;
import static net.esyo.tileengine.engine.core.utils.AngleUtils.A180;

/**
 *
 * @author rpetit
 */
public class Vect2 {

    private Pt x;
    private Pt y;

    private Vect2(final Pt x, final Pt y) {
        this.x = x;
        this.y = y;
    }

    private Vect2(final double x, final double y) {
        this.x = Pt.of(x);
        this.y = Pt.of(y);
    }

    public static Vect2 of(final Pt x, final Pt y) {
        return new Vect2(x, y);
    }

    public static Vect2 of(final double x, final Pt y) {
        return new Vect2(Pt.of(x), y);
    }

    public static Vect2 of(final Pt x, final double y) {
        return new Vect2(x, Pt.of(y));
    }

    public static Vect2 of(final double x, final double y) {
        return new Vect2(x, y);
    }

    public static Vect2 fromRadian(final Pt angle) {
        return Vect2.of(Math.cos(angle.asDouble()), Math.sin(angle.asDouble()));
    }

    public static Vect2 fromRadian(final double angle) {
        return fromRadian(Pt.of(angle));
    }

    public static Vect2 of(final Pt d) {
        return of(d, d);
    }

    public static Vect2 of(final double d) {
        return of(Pt.of(d));
    }

    public static Vect2 zero() {
        return of(0, 0);
    }

    /**
     * Muttable
     *
     * @return me with x and y values updated to 0
     */
    public Vect2 asZero() {
        this.x(0);
        this.y(0);
        return this;
    }

    /**
     *
     * @return a copy of the x value
     */
    public final Pt x() {
        return this.x.copy();
    }

    /**
     *
     * @return a copy of the y value
     */
    public final Pt y() {
        return this.y.copy();
    }

    /**
     * Muttable
     *
     * @param d
     * @return me with the x value updated
     */
    public Vect2 x(final Pt d) {
        this.x = d;
        return this;
    }

    /**
     * Muttable
     *
     * @param d
     * @return me with the x value updated
     */
    public Vect2 x(final double d) {
        this.x = Pt.of(d);
        return this;
    }

    /**
     * Muttable
     *
     * @param d
     * @return me with the y value updated
     */
    public Vect2 y(final Pt d) {
        this.y = d;
        return this;
    }

    /**
     * Muttable
     *
     * @param d
     * @return me with the y value updated
     */
    public Vect2 y(final double d) {
        this.y = Pt.of(d);
        return this;
    }

    /**
     *
     * @return a copy of me
     */
    public Vect2 copy() {
        return Vect2.of(this.x.copy(), this.y.copy());
    }

    /**
     * Muttable
     *
     * @param other
     * @return me with the x and y values updated (added the other's x and y
     * values)
     */
    public Vect2 add(final Vect2 other) {
        this.x.add(other.x);
        this.y.add(other.y);
        return this;
    }

    /**
     * Muttable
     *
     * @param d
     * @return me with the x and y values updated (added the other's x and y
     * values)
     */
    public Vect2 add(final double d) {
        this.x.add(d);
        this.y.add(d);
        return this;
    }

    public Vect2 add(double x, double y) {
        this.x.add(x);
        this.y.add(y);
        return this;
    }

    /**
     * Muttable
     *
     * @param d
     * @return me with the x and y values updated (added the other's x and y
     * values)
     */
    public Vect2 add(final Pt d) {
        this.x.add(d);
        this.y.add(d);
        return this;
    }

    /**
     * Muttable
     *
     * @param other
     * @return me with the x and y values updated (substracted the other's x and
     * y values)
     */
    public Vect2 sub(final Vect2 other) {
        this.x.sub(other.x);
        this.y.sub(other.y);
        return this;
    }

    /**
     * Muttable
     *
     * @param d
     * @return me with the x and y values updated (substracted the other's x and
     * y values)
     */
    public Vect2 sub(final double d) {
        this.x.sub(d);
        this.y.sub(d);
        return this;
    }

    /**
     * Muttable
     *
     * @param d
     * @return me with the x and y values updated (substracted the other's x and
     * y values)
     */
    public Vect2 sub(final Pt d) {
        this.x.sub(d);
        this.y.sub(d);
        return this;
    }

    /**
     * Muttable
     *
     * @param other
     * @return me with the x and y values updated (multiplied the other's x and
     * y values)
     */
    public Vect2 mul(final Vect2 other) {
        this.x.mul(other.x);
        this.y.mul(other.y);
        return this;
    }

    /**
     * Muttable
     *
     * @param d
     * @return me with the x and y values updated (multiplied the other's x and
     * y values)
     */
    public Vect2 mul(final double d) {
        this.x.mul(d);
        this.y.mul(d);
        return this;
    }

    /**
     * Muttable
     *
     * @param d
     * @return me with the x and y values updated (multiplied the other's x and
     * y values)
     */
    public Vect2 mul(final Pt d) {
        this.x.mul(d);
        this.y.mul(d);
        return this;
    }

    /**
     * Muttable
     *
     * @param other
     * @return me with the x and y values updated (divided the other's x and y
     * values)
     */
    public Vect2 div(final Vect2 other) {
        this.x.div(other.x);
        this.y.div(other.y);
        return this;
    }

    /**
     * Muttable
     *
     * @param d
     * @return me with the x and y values updated (divided the other's x and y
     * values)
     */
    public Vect2 div(final double d) {
        this.x.div(d);
        this.y.div(d);
        return this;
    }

    /**
     * Muttable
     *
     * @param d
     * @return me with the x and y values updated (divided the other's x and y
     * values)
     */
    public Vect2 div(final Pt d) {
        this.x.div(d);
        this.y.div(d);
        return this;
    }

    /**
     *
     * @return the vector length (or called magnitude or size)
     */
    public final Pt length() {
        return x().square().add(y().square()).sqrt();
    }

    /**
     *
     * @param point another vector
     * @return the vector's distance
     */
    public final Pt distance(final Vect2 point) {
        if (point != null) {
            final Pt px = point.x().sub(this.x);
            final Pt py = point.y().sub(this.y);
            return px.square().add(py.square()).sqrt();
        }
        return Pt.of(0);
    }

    /**
     * Muttable
     *
     * @return me normalized
     */
    public Vect2 normalize() {
        final Pt l = length();
        this.x.div(l);
        this.y.div(l);
        return this;
    }

    /**
     *
     * @param point
     * @return the angle between the 2 vectors
     */
    public Pt angle(final Vect2 point) {
        final Pt dot = dotProduct(point);
        final Pt a = length();
        final Pt b = point.length();
        return dot.div(a.mul(b)).acos().degrees();
    }

    /**
     *
     * @return angle radian from horizontal right
     * @link http://www.onemathematicalcat.org/Math/Precalculus_obj/horizVertToDirMag.htm
     */
    public double angle() {
        return (x.negative() ? A180 : A0) + Math.atan(y.asDouble() / x.asDouble());
    }

    /**
     *
     * @param point
     * @return the vector's dot product
     * @link http://onlinemschool.com/math/library/vector/multiply/#h4
     */
    public Pt dotProduct(final Vect2 point) {
        return point.x().mul(this.x).add(point.y().mul(this.y));
    }

    /**
     *
     * @return the direction angle in radian
     */
    public Pt direction() {
        return y().div(this.x).atan();
    }

    @Override
    public String toString() {
        return this.x.asInt() + "," + this.y.asInt();
    }

    /**
     * Muttable
     *
     * @param x
     * @param y
     * @return me with x and y values updated to the new parameters
     */
    public Vect2 to(final Pt x, final Pt y) {
        this.x = x;
        this.y = y;
        return this;
    }

    /**
     * Muttable
     *
     * @param x
     * @param y
     * @return me with x and y values updated to the new parameters
     */
    public Vect2 to(final double x, final double y) {
        this.x = Pt.of(x);
        this.y = Pt.of(y);
        return this;
    }

    public Vect2 half() {
        this.x.half();
        this.y.half();
        return this;
    }

}
