package net.esyo.tileengine.engine.core.paths;

import java.util.HashSet;
import java.util.Set;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author rpetit
 * @link https://gamedevelopment.tutsplus.com/tutorials/understanding-steering-behaviors-path-following--gamedev-8769
 */
public class FreePath implements Path {

    private final Set<Vect2> nodes = new HashSet<>();

    //target  = nodes[currentNode];

    public FreePath add(final Vect2 node) {
        this.nodes.add(node);
        return this;
    }

    public Set<Vect2> nodes() {
        return this.nodes();
    }

    @Override
    public double y(double x) {
        /*
        if (distance(position, target) <= 10) {
                currentNode += 1;

                if (currentNode >= nodes.length) {
                    currentNode = nodes.length - 1;
                }
            }
*/
        return x;

    }

    /*
    private function distance(a :Object, b :Object) :Number {
        return Math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
    }
     */
}
