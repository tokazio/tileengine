package net.esyo.tileengine.engine.core;

/**
 *
 * @author romainpetit
 */
public interface Movable {

    <T extends Movable> T velocity(final Vect2 velocity);

    Vect2 velocity();

    <T extends Movable> T acceleration(final Vect2 acceleration);

    Vect2 acceleration();
}
