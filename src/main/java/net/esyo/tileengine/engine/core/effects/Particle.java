package net.esyo.tileengine.engine.core.effects;

import net.esyo.tileengine.engine.core.Movable;
import net.esyo.tileengine.engine.core.Positionable;
import net.esyo.tileengine.engine.core.Renderable;
import net.esyo.tileengine.engine.core.Updatable;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author rpetit
 */
public interface Particle extends Positionable, Movable, Renderable, Updatable {

    <T extends Particle> T applyForce(final Vect2 velocity);
}
