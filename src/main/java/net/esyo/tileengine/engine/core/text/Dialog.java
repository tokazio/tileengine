package net.esyo.tileengine.engine.core.text;

import javafx.geometry.Point2D;
import javafx.scene.input.KeyEvent;
import net.esyo.tileengine.engine.core.AbstractAnimable;
import net.esyo.tileengine.engine.core.AnimatedRect;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.OnKey;
import net.esyo.tileengine.engine.core.Positionable;
import net.esyo.tileengine.engine.core.Renderable;
import net.esyo.tileengine.engine.core.Updatable;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Dialog extends AbstractAnimable implements Positionable, Updatable, Renderable, OnKey {

    private final AnimatedRect ar;
    private final LetterByLetterText t;

    public Dialog(final Point2D position, final String text) {
        t = new LetterByLetterText(text, Vect2.of(20, 385), Vect2.of(482, 80));
	ar = new AnimatedRect(position.getX(), 10, position.getY(), 380, 0, 492, 0, 100, 500);
	ar.end = () -> {
	    if (isPlaying()) {
		t.play();
	    }
	};
    }

    @Override
    public Dialog play(final boolean loop) {
	ar.play(false);
	return super.play(loop);
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        t.update(screenSize, elapsedTime);
        ar.update(screenSize, elapsedTime);
    }

    @Override
    public void render(final Graphics gc) {
	ar.render(gc);
	t.render(gc);
    }

    @Override
    public <T extends Positionable> T position(final Vect2 point) {
	return (T) this;
    }

    @Override
    public Vect2 position() {
        throw new RuntimeException("No position for a Dialog");
    }

    @Override
    public void onKeyPress(final KeyEvent k) {
	t.onKeyPress(k);
    }

    @Override
    public void onKeyUp(final KeyEvent k) {
	t.onKeyUp(k);
    }
}
