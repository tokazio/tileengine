package net.esyo.tileengine.engine.core;

/**
 *
 * @author romainpetit
 */
public interface Explodable {
    
    void explode();
    
    boolean isExploded();
    
    boolean canExplode();
}
