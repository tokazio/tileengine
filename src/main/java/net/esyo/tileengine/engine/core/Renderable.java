package net.esyo.tileengine.engine.core;

/**
 * Object that can be rendered on a GraphicsContext.
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public interface Renderable {

    void render(final Graphics gc);

    /*<T extends Renderable> T position(final Vect2 point);

    Vect2 position();*/
}
