package net.esyo.tileengine.engine.core.paths;

/**
 *
 * @author rpetit
 */
public class LinearPath implements Path {

    private final double y;

    public LinearPath(final double y) {
        this.y = y;
    }

    @Override
    public double y(double x) {
        return y;
    }
}
