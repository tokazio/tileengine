package net.esyo.tileengine.engine.core;

/**
 *
 * @author rpetit
 */
public interface Collectable {

    boolean canRemove();
}
