package net.esyo.tileengine.engine.core.utils;

public class LongValue {

    private long value;

    public LongValue(long i) {
	value = i;
    }

    public long getValue() {
	return value;
    }

    public void setValue(long value) {
	this.value = value;
    }
}
