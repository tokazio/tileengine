package net.esyo.tileengine.engine.core.effects;

import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.utils.ColorUtils;

/**
 *
 * @author rpetit
 */
public class CircleParticleEmitter extends AbstractLivingParticleEmitter<CircleParticle> {

    public CircleParticleEmitter(int number) {
        super(number);
    }

    public CircleParticleEmitter(Vect2 position, int number) {
        super(position, number);
    }

    @Override
    public CircleParticle resetParticle(CircleParticle p) {
        p = super.resetParticle(p);
        p = p.timeToLive(2000)
                .velocity(Vect2.of(Math.random() * 4 - 2, Math.random() * 4 - 2))
                .acceleration(Vect2.of(0, 0.05));
        p = p.color(ColorUtils.random())
                .decay((int) (Math.random() * 300 + 200))
                .opacity(Math.random())
                .size(Math.random() * 10);
        return p;
    }

    @Override
    public CircleParticle createParticle() {
        return new CircleParticle();
    }

    @Override
    public Vect2 position() {
        return this.position;
    }

}
