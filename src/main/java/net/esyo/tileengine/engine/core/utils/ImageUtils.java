package net.esyo.tileengine.engine.core.utils;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public final class ImageUtils {

    private ImageUtils(){
	//Hide constructor
    }

    public static Color pickColor(final Image image, final Vect2 point) {
        final PixelReader pixelReader = image.getPixelReader();
        return pixelReader.getColor(point.x().asInt(), point.y().asInt());
    }

    public static Image setTransparentColor(final Image image, final Color col) {
        final PixelReader pixelReader = image.getPixelReader();
	// Create WritableImage
	final WritableImage wImage = new WritableImage(
              		(int) image.getWidth(),
		(int) image.getHeight());
        final PixelWriter pixelWriter = wImage.getPixelWriter();
	// Determine the color of each pixel in a specified row
	for (int readY = 0; readY < image.getHeight(); readY++) {
	    for (int readX = 0; readX < image.getWidth(); readX++) {
		if (pixelReader.getColor(readX, readY).equals(col)) {
		    pixelWriter.setColor(readX, readY, Color.TRANSPARENT);
		} else {
		    pixelWriter.setColor(readX, readY, pixelReader.getColor(readX, readY));
		}
	    }
	}
	return wImage;
    }

    public static Image white(final Image image) {
        final PixelReader pixelReader = image.getPixelReader();
        final WritableImage wImage = new WritableImage(
                (int) image.getWidth(),
                (int) image.getHeight());
        final PixelWriter pixelWriter = wImage.getPixelWriter();
        for (int y = 0; y < image.getHeight(); y += 1) {
            for (int x = 0; x < image.getWidth(); x += 1) {
                if (pixelReader.getColor(x, y).getOpacity() != 0) {
                    pixelWriter.setColor(x, y, Color.WHITE);
                }
            }
        }
        return wImage;
    }
}
