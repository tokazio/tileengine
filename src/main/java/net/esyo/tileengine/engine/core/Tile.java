package net.esyo.tileengine.engine.core;

/**
 * Tile (static).
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Tile {

    private final Vect2 size;
    private final Vect2 position;

    public Tile(final int x, final int y, final int width, final int height) {
        this(Vect2.of(x, y), Vect2.of(width, height));
    }

    public Tile(final Vect2 position, final Vect2 size) {
	this.position = position;
	this.size = size;
    }

    public Vect2 position() {
	return position;
    }

    public Vect2 size() {
	return size;
    }
}
