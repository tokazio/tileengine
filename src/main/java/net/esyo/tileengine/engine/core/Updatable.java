package net.esyo.tileengine.engine.core;

/**
 * Object that can be updated in timed loop.
 *
 * The update method will handle transformations/calculations of the object.
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
@FunctionalInterface
public interface Updatable {

    void update(final Vect2 screenSize, final long elapsedTime);
}
