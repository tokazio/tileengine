package net.esyo.tileengine.engine.core;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public final class Rect2 {

    private final Vect2 position;
    private final Vect2 size;

    private Rect2(final Rect2 org) {
        this.position = org.position.copy();
        this.size = org.size.copy();
    }

    private Rect2(final Vect2 position, final Vect2 size) {
        this.position = position.copy();
        this.size = size.copy();
    }

    private Rect2(final double x, final double y, final double width, final double height) {
        this(Vect2.of(x, y), Vect2.of(width, height));
    }

    private Rect2(final Pt x, final Pt y, final Pt width, final Pt height) {
        this(Vect2.of(x, y), Vect2.of(width, height));
    }

    public static Rect2 of(final Vect2 position, final Vect2 size) {
        return new Rect2(position.copy(), size.copy());
    }

    public static Rect2 of(final Vect2 position, final double width, double height) {
        return new Rect2(position.copy(), Vect2.of(width, height));
    }

    public static Rect2 of(final double left, double top, final Vect2 size) {
        return new Rect2(Vect2.of(left, top), size.copy());
    }

    public static Rect2 of(final double x, final double y, final double width, final double height) {
        return new Rect2(x, y, width, height);
    }

    public static Rect2 of(final Pt x, final Pt y, final Pt width, final Pt height) {
        return new Rect2(x, y, width, height);
    }

    public Rect2 copy() {
        return new Rect2(this);
    }

    public Vect2 position() {
        return this.position;
    }

    public Vect2 size() {
        return this.size;
    }

    public Pt left() {
        return this.position.x();
    }

    public Pt top() {
        return this.position.y();
    }

    public Pt width() {
        return this.size.x();
    }

    public Pt height() {
        return this.size.y();
    }

    public Pt right() {
        return this.position.x().add(this.size().x());
    }

    public Pt bottom() {
        return this.position.y().add(this.size().y());
    }

    @Override
    public String toString() {
        return "Rect2 from " + this.position + " to " + this.position.add(this.size);
    }

    public boolean intersect(final double x, final double y, final double w, final double h) {
        return intersect(Rect2.of(x, y, w, h));
    }

    public boolean intersect(final Rect2 rect) {
        return rect.position.x().add(rect.size.x()).greaterThan(this.position.x())
                && rect.position.x().lowerThan(this.position.x().add(this.size.x()))
                && rect.position.y().lowerThan(this.position.y().add(this.size.y()))
                && rect.size.y().add(rect.position.y()).greaterThan(this.position.y());
    }
}
