package net.esyo.tileengine.engine.core;

/**
 * Sprite (static) from a tileset.
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Sprite implements Positionable, Renderable {

    protected final TileSet tileSet;
    protected int defaultTileCode;
    private Vect2 position;
    protected Vect2 size;
    protected boolean visible = true;

    public Sprite(final TileSet tiles, final int tileCode) {
        this.tileSet = tiles;
        this.size = tiles.size();
        this.defaultTileCode = tileCode;
        this.position = Vect2.of(0, 0);
    }

    public Sprite(final TileSet tiles, final int tileCode, final Vect2 position) {
        this(tiles, tileCode);
        this.position = position.copy();
    }

    public int defaultTileCode() {
        return this.defaultTileCode;
    }

    public void defaultTileCode(final int i) {
        this.defaultTileCode = i;
    }

    public TileSet tileSet() {
        return this.tileSet;
    }

    /**
     * @return a copy of the position
     */
    @Override
    public Vect2 position() {
        return this.position;
    }

    /**
     * be carefull, size is mutable, use <code>.copy()</code>
     * @return width, height size
     */
    public Vect2 size() {
        return this.size;
    }

    @Override
    public <T extends Positionable> T position(final Vect2 point) {
        this.position = point.copy();
        return (T) this;
    }

    public <T extends Renderable> T position(final double x, final double y) {
        this.position = Vect2.of(x, y);
        return (T) this;
    }

    @Override
    public void render(final Graphics gc) {
        if (isVisible()) {
            tileSet().render(gc, this.defaultTileCode(), this.position);
        }
    }

    /**
     * if not <code>visible</code>, it's not rendered in the render method
     *
     * @param visible
     */
    public void visible(final boolean visible) {
        this.visible = visible;
    }

    /**
     * if not <code>visible</code>, it's not rendered in the render method
     *
     * @return
     */
    public boolean isVisible() {
        return this.visible;
    }

    /**
     *
     * @return a <code>Rect2</code> with left,top coordinates and right,bottom
     * coordinates (position,position+size)
     */
    public Rect2 getBounds() {
        return Rect2.of(position, position.copy().add(size));
    }

    @Override
    public String toString() {
        return "#" + this.defaultTileCode() + " @ " + this.getBounds();
    }
}
