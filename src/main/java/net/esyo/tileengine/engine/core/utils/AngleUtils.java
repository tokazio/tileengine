package net.esyo.tileengine.engine.core.utils;

/**
 *
 * @author rpetit
 */
public final class AngleUtils {

    private AngleUtils() {
        //hide constructor
    }

    public static final double A0 = 0;
    public static final double A45 = Math.PI / 4;
    public static final double A90 = Math.PI / 2;
    public static final double A135 = Math.PI / 4 * 3;
    public static final double A180 = Math.PI;
    public static final double A225 = Math.PI / 4 * 5;
    public static final double A270 = Math.PI / 2 * 3;
    public static final double A315 = Math.PI / 4 * 7;
    public static final double A360 = Math.PI * 2;

}
