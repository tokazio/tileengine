package net.esyo.tileengine.engine.core;

/**
 * Use on object that can be animated.
 * Play, pause, resume, stop animation.
 * Give onEnd callback event after the animation.
 *
 * See Animated class as direct implementation of this interface.
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public interface Animable {

    <T extends Animable> T play(final boolean loop);

    <T extends Animable> T pause();

    <T extends Animable> T resume();

    <T extends Animable> T stop();

    <T extends Animable> T onEnd();

    boolean isPlaying();
}
