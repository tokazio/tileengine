package net.esyo.tileengine.engine.core.utils;

import javafx.scene.paint.Color;

/**
 *
 * @author rpetit
 */
public final class ColorUtils {

    public static Color of(double d, double opacity) {
        return new Color(d, d, d, opacity);
    }

    public static Color of(int r, int g, int b) {
        return new Color(r / 256d, g / 256d, b / 256d, 1);
    }

    public static Color of(Color color, double opacity) {
        return new Color(color.getRed(), color.getGreen(), color.getBlue(), opacity);
    }

    public static Color of(double d) {
        return new Color(d, d, d, 1);
    }

    private ColorUtils() {
        //hide
    }

    public static Color random() {
        return new Color(Math.random(), Math.random(), Math.random(), 1);
    }
}
