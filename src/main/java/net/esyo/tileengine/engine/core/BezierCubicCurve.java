package net.esyo.tileengine.engine.core;

/**
 *
 * @author rpetit
 * @link http://devmag.org.za/2011/04/05/bzier-curves-a-tutorial/
 * @link http://cubic-bezier.com/#.16,.66,.86,.09
 * @link http://easings.net/fr
 *
 */
public class BezierCubicCurve {

    private final Vect2 p0 = Vect2.of(0, 0);
    private final Vect2 p1;
    private final Vect2 p2;
    private final Vect2 p3 = Vect2.of(1, 1);

    public BezierCubicCurve(Vect2 p1, Vect2 p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public Vect2 point(double t) {
        double u = 1 - t;
        double tt = t * t;
        double uu = u * u;
        double uuu = uu * u;
        double ttt = tt * t;
        final Vect2 p = p0.copy().mul(uuu);//first term
        p.add(p1.copy().mul(3 * uu * t)); //second term
        p.add(p2.copy().mul(3 * u * tt)); //third term
        p.add(p3.copy().mul(ttt)); //fourth term
        return p;
    }

}
