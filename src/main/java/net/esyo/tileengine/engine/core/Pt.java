package net.esyo.tileengine.engine.core;

/**
 *
 * @author romainpetit
 */
public class Pt {

    private double value;

    private Pt(final double value){
        this.value = value;
    }

    public static Pt of(final double value){
        return new Pt(value);
    }

    /**
     * Muttable
     * @param value
     * @return
     */
    public Pt to(final double value){
        this.value = value;
        return this;
    }

    public double asDouble() {
        return this.value;
    }

    public int asInt() {
        return (int) this.value;
    }

    /**
     * Muttable
     * @param other
     * @return
     */
    public Pt add(final Pt other) {
        this.value+=other.value;
        return this;
    }

    /**
     * Muttable
     * @param d
     * @return
     */
    public Pt add(final double d) {
        return add(Pt.of(d));
    }

    /**
     * Muttable
     * @param other
     * @return
     */
    public Pt sub(final Pt other) {
        this.value-=other.value;
        return this;
    }

    /**
     * Muttable
     * @param d
     * @return
     */
    public Pt sub(final double d) {
        return sub(Pt.of(d));
    }

    /**
     * Muttable
     * @param other
     * @return
     */
    public Pt mul(final Pt other) {
        this.value*=other.value;
        return this;
    }

    /**
     * Muttable
     * @param d
     * @return
     */
    public Pt mul(final double d) {
        return mul(Pt.of(d));
    }

    /**
     * Muttable
     * @param other
     * @return
     */
    public Pt div(final Pt other) {
        this.value/=other.value;
        return this;
    }

    /**
     * Muttable
     * @param d
     * @return
     */
    public Pt div(final double d) {
        return div(Pt.of(d));
    }

    /**
     * Muttable
     * @return
     */
    public Pt square() {
        this.value = this.value * this.value;
        return this;
    }

    /**
     *
     * @return new Pt with the same value
     */
    public Pt copy() {
        return new Pt(this.value);
    }

    /**
     * Muttable
     * @return
     */
    public Pt sqrt(){
        this.value = Math.sqrt(this.value);
        return this;
    }

    /**
     * Muttable
     * @return
     */
    public Pt atan() {
        this.value = Math.atan(this.value);
        return this;
    }

    /**
     * Muttable
     * @return
     */
    public Pt acos() {
        this.value = Math.acos(this.value);
        return this;
    }

    /**
     * Muttable
     * @return
     */
    public Pt asin() {
        this.value = Math.asin(this.value);
        return this;
    }

    /**
     * Muttable
     * @return
     */
    public Pt degrees() {
        this.value = Math.toDegrees(this.value);
        return this;
    }

    /**
     * Muttable
     * @return
     */
    public Pt radians() {
        this.value = Math.toRadians(this.value);
        return this;
    }

    public boolean greaterThan(final double max) {
        return this.value > max;
    }

    public boolean greaterThan(final Pt max) {
        return this.value > max.value;
    }

    public boolean lowerThan(final double max) {
        return this.value < max;
    }

    public boolean lowerThan(final Pt max) {
        return this.value < max.value;
    }

    public boolean lowerOrEquals(final double max) {
        return this.value <= max;
    }

    public boolean lowerOrEquals(final Pt max) {
        return this.value <= max.value;
    }

    public boolean greaterOrEquals(final double max) {
        return this.value >= max;
    }

    public boolean greaterOrEquals(final Pt max) {
        return this.value >= max.value;
    }

    public boolean equals(final double max) {
        return this.value == max;
    }

    public boolean equalTo(final Pt max) {
        return this.value == max.value;
    }

    public boolean positive() {
        return this.value>0;
    }

    public boolean negative() {
        return this.value<0;
    }

    public boolean zero() {
        return this.value==0;
    }

    /**
     * Muttable
     *
     * @return
     */
    public Pt floor() {
        this.value = Math.floor(this.value);
        return this;
    }

    /**
     * Muttable
     *
     * @return
     */
    public Pt random() {
        this.value = Math.random() * this.value;
        return this;
    }

    /**
     * Muttable
     *
     * @return
     */
    public Pt inverse() {
        this.value = -this.value;
        return this;
    }

    public boolean notZero() {
        return this.value != 0;
    }

    /**
     * Muttable
     *
     * @return
     */
    public Pt half() {
        this.value = this.value / 2d;
        return this;
    }

    @Override
    public String toString() {
        return Double.toString(value);
    }
}
