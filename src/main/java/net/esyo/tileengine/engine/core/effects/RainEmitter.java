package net.esyo.tileengine.engine.core.effects;

import java.util.ArrayList;
import java.util.List;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Positionable;
import net.esyo.tileengine.engine.core.Renderable;
import net.esyo.tileengine.engine.core.Updatable;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author rpetit
 */
public class RainEmitter implements Positionable, Renderable, Updatable {

    private final List<RainParticle> rains = new ArrayList<>();

    public RainEmitter(final Vect2 screenSize, final int number) {
        for (int i = 0; i < number; i++) {
            rains.add(new RainParticle(screenSize));
        }
    }

    @Override
    public void render(Graphics gc) {
        rains.forEach(r -> r.render(gc));
    }

    @Override
    public <T extends Positionable> T position(Vect2 point) {
        return (T) this;
    }

    @Override
    public Vect2 position() {
        throw new UnsupportedOperationException("No position for a RainEmitter.");
    }

    @Override
    public void update(final Vect2 screenSize, long elapsedTime) {
        rains.forEach(r -> r.update(screenSize, elapsedTime));
    }

    public void update(final Vect2 screenSize, final long elapsedTime, final double x, final double y) {
        rains.forEach(r -> r.update(screenSize, elapsedTime, x * 10, y * 10));
    }

}
