package net.esyo.tileengine.engine.core;

/**
 * Go from 'start' to 'end' in 'time' ms. Interpolation is a double type.
 * Handle the negative values and 'end' value can be lower than 'start' value.
 *
 * Interpolation is Animable, you can 'Play', 'Pause', 'Resume', 'Stop' it.
 *
 * The 'update' method compute the value and return it.
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class LinearInterpolation extends AbstractAnimable{

    private double startValue;
    private double curValue;
    private double endValue;
    private double len;
    private int duration;
    private double cumul;

    public LinearInterpolation() {
	this.startValue = 0;
	this.curValue = 0;
	this.endValue = 0;
	this.cumul = 0;
	this.duration = 0;
	this.len = 0;
    }

    public LinearInterpolation(final double start, final double end, final int duration) {
	set(start, end, duration);
    }

    public final void set(final double start, final double end, final int duration) {
	this.startValue = start;
	this.curValue = start;
	this.endValue = end;
	this.cumul = 0;
	this.duration = duration;
	this.len = Math.abs(end - start);
    }

    public double update(final long time) {
	if (playing) {
	    cumul += time;
	    if (endValue > startValue) {
		if (curValue < endValue) {
		    curValue = startValue + (cumul / duration) * len;
		} else {
		    playing = false;
		    if (super.end != null) {
			super.end.call();
		    }
		    if (looping) {
			playing = true;
			curValue = startValue;
			cumul = 0;
		    } else {
			curValue = endValue;
		    }
		}
	    } else {
		if (curValue > endValue) {
		    curValue = startValue - (cumul / duration) * len;
		} else {
		    playing = false;
		    if (super.end != null) {
			super.end.call();
		    }
		    if (looping) {
			playing = true;
			curValue = startValue;
			cumul = 0;
		    } else {
			curValue = endValue;
		    }
		}
	    }
	}
	return curValue;
    }
}
