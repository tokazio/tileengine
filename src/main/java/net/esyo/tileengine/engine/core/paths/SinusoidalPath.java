package net.esyo.tileengine.engine.core.paths;

/**
 *
 * @author rpetit
 */
public class SinusoidalPath implements Path {

    @Override
    public double y(double x) {
        return Math.sin(x);
    }
}
