package net.esyo.tileengine.engine.core;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.Effect;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.ArcType;
import javafx.scene.text.Font;
import javafx.scene.transform.Rotate;
import net.esyo.tileengine.engine.core.utils.ColorUtils;

/**
 *
 * @author rpetit
 */
public class Graphics {

    /**
     * The engine GraphicsContext
     */
    private final GraphicsContext gc;
    private final Vect2 screenSize;

    public Graphics(final GraphicsContext gc, final Vect2 screenSize) {
        this.gc = gc;
        this.screenSize = screenSize.copy();
    }

    public Vect2 screenSize() {
        return this.screenSize;
    }

    public void setFill(final int r, final int g, final int b) {
        gc.setFill(new Color(r / 256f, g / 256f, b / 256f, 1));
    }

    public void setFill(final int r, final int g, final int b, final int a) {
        gc.setFill(new Color(r / 256f, g / 256f, b / 256f, a / 256f));
    }

    public void setStroke(final int r, final int g, final int b) {
        gc.setStroke(new Color(r / 256f, g / 256f, b / 256f, 1));
    }

    public void setStroke(final int r, final int g, final int b, final int a) {
        gc.setStroke(new Color(r / 256f, g / 256f, b / 256f, a / 256f));
    }

    public void setFill(final double d) {
        gc.setFill(ColorUtils.of(d));
    }

    private double bound(double d) {
        return d < 0 ? 0 : d > 1 ? 1 : d;
    }

    public void setFill(final double d, final double opacity) {
        gc.setFill(ColorUtils.of(d, bound(opacity)));
    }

    public void setFill(final Color color, final double opacity) {
        gc.setFill(ColorUtils.of(color, bound(opacity)));
    }

    public void setStroke(final double d) {
        gc.setStroke(ColorUtils.of(d));
    }

    public void setStroke(final double d, final double opacity) {
        gc.setStroke(ColorUtils.of(d, bound(opacity)));
    }

    public void setStroke(final Color color, final double opacity) {
        gc.setStroke(ColorUtils.of(color, bound(opacity)));
    }

    public void setFont(Font font) {
        gc.setFont(font);
    }

    public void clearRect(double i, double i0, double i1, double i2) {
        gc.clearRect(i, i0, i1, i2);
    }

    public void strokeText(String string, double x, double d) {
        gc.strokeText(string, x, d);
    }

    public void drawImage(Image tilesetImage, double x, double y, double x0, double y0, double x1, double y1, double x2, double y2) {
        gc.drawImage(tilesetImage, x, y, x0, y0, x1, y1, x2, y2);
    }

    public void setEffect(final Effect effect){
        gc.setEffect(effect);
    }
    
    public void setLineWidth(double i) {
        gc.setLineWidth(i);
    }

    public void setFill(Paint paint) {
        gc.setFill(paint);
    }

    public void setStroke(Paint paint) {
        gc.setStroke(paint);
    }

    public void fillRoundRect(double d, double d0, double d1, double d2, int i, int i0) {
        gc.fillRoundRect(d, d0, d1, d2, i, i0);
    }

    public void strokeRoundRect(double xp, double yp, double wp, double hp, int i, int i0) {
        gc.strokeRoundRect(xp, yp, wp, hp, i, i0);
    }

    public void fillText(String string, double d, double d0) {
        gc.fillText(string, d, d0);
    }

    public void strokeRect(double d, double d0, double x, double y) {
        gc.strokeRect(d, d0, x, y);
    }

    public void fillOval(double x, double y, double size, double size0) {
        gc.fillOval(x, y, size, size0);
    }

    public void strokeLine(double d, double d0, double d1, double d2) {
        gc.strokeLine(d, d0, d1, d2);
    }

    public void strokeLine(Vect2 position1, Vect2 position2) {
        gc.strokeLine(position1.x().asDouble(), position1.y().asDouble(), position2.x().asDouble(), position2.y().asDouble());
    }

    public void strokeLine(Vect2 position1, double x2, double y2) {
        gc.strokeLine(position1.x().asDouble(), position1.y().asDouble(), x2, y2);
    }

    public void fillRect(double i, double i0, double x, double y) {
        gc.fillRect(i, i0, x, y);
    }

    public void fillRect(Vect2 size) {
        gc.fillRect(0, 0, size.x().asDouble(), size.y().asDouble());
    }

    public void fillRect(Vect2 position, Vect2 size) {
        gc.fillRect(position.x().asDouble(), position.y().asDouble(), size.x().asDouble(), size.y().asDouble());
    }

    public Font getFont() {
        return gc.getFont();
    }

    public void setGlobalBlendMode(BlendMode blendMode) {
        gc.setGlobalBlendMode(blendMode);
    }

    public void save() {
        gc.save();
    }

    public void restore() {
        gc.restore();
    }

    public void rotate(double angle, double x, double y) {
        final Rotate r = new Rotate(angle, x, y);
        gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
    }

    public void strokeOval(double d, double d0, int i, int i0) {
        gc.strokeOval(d, d0, i, i0);
    }

    /**
     *
     * @param bounds
     * @param startAngle in degree
     * @param arcExtent in degree
     */
    public void strokeArc(Rect2 bounds, double startAngle, double arcExtent) {
        gc.strokeArc(bounds.left().asDouble(), bounds.top().asDouble(), bounds.width().asDouble(), bounds.height().asDouble(), startAngle, arcExtent, ArcType.OPEN);
    }

    public void fillCircle(Vect2 v, double radius) {
        gc.fillOval(v.x().sub(radius / 2).asDouble(), v.y().sub(radius / 2).asDouble(), radius, radius);
    }

    public void strokeCircle(Vect2 v, double radius) {
        gc.strokeOval(v.x().sub(radius / 2).asDouble(), v.y().sub(radius / 2).asDouble(), radius, radius);
    }

}
