package net.esyo.tileengine.engine.core.paths;

/**
 *
 * @author rpetit
 */
public interface Path {

    double y(double x);
}
