package net.esyo.tileengine.engine.core;

import javafx.scene.paint.Color;

/**
 * Animate the four points of a 2D rect (one interpolation for each).
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class AnimatedRect extends AbstractAnimable implements Positionable, Renderable, Updatable {

    private final LinearInterpolation x;
    private final LinearInterpolation y;
    private final LinearInterpolation w;
    private final LinearInterpolation h;
    private double xp;
    private double yp;
    private double wp;
    private double hp;

    public AnimatedRect(final double a, final double aa, final double b, final double bb, final double c, final double cc, final double d, final double dd, final int duration) {
        this.x = new LinearInterpolation(a, aa, duration);
        this.y = new LinearInterpolation(b, bb, duration);
        this.w = new LinearInterpolation(c, cc, duration);
        this.h = new LinearInterpolation(d, dd, duration);
    }

    @Override
    public void render(final Graphics gc) {
	gc.setStroke(Color.WHITE);
	gc.setFill(Color.BLACK);
	gc.setLineWidth(2);
	gc.fillRoundRect(xp-2, yp-2, wp+4, hp+4, 14, 14);
	gc.strokeRoundRect(xp, yp, wp, hp, 10, 10);
    }

    @Override
    public <T extends Positionable> T position(final Vect2 point) {
	return (T) this;
    }

    @Override
    public Vect2 position() {
        throw new RuntimeException("No position for an AnimatedRect");
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
	if (isPlaying()) {
	    xp = x.update(elapsedTime);
	    yp = y.update(elapsedTime);
	    wp = w.update(elapsedTime);
	    hp = h.update(elapsedTime);
	    if(!x.isPlaying() && !y.isPlaying() && !w.isPlaying() && !h.isPlaying()){
		stop();
		onEnd();
	    }
	}
    }

    @Override
    public AnimatedRect play(final boolean loop) {
        this.x.play(false);
        this.y.play(false);
        this.w.play(false);
        this.h.play(false);
	return super.play(loop);
    }

    //@todo: pause, resume
}
