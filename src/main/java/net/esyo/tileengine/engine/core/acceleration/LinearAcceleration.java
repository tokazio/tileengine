package net.esyo.tileengine.engine.core.acceleration;

/**
 *
 * @author rpetit
 */
public class LinearAcceleration implements Acceleration {

    private double value;

    public Acceleration accelerate() {
        this.value += 1;
        return this;
    }

    public double value() {
        return this.value;
    }

}
