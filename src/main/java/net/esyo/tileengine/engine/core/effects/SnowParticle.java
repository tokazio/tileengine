package net.esyo.tileengine.engine.core.effects;

import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author rpetit
 */
public class SnowParticle extends AbstractParticle {

    private static final int DEVIATION = 5;

    private final double size = Math.random() * 4 + 2;
    private final double opacity = Math.random() * (size / 10) + 0.1;
    private final Vect2 screenSize;
    private final Vect2 correction = Vect2.zero();

    public SnowParticle(final Vect2 screenSize) {
        this.screenSize = screenSize;
        reset();
    }

    private void reset() {
        this.position(randomStart());
        this.velocity(randomVelocity());
        this.acceleration(randomAcceleration());
    }

    @Override
    public void render(final Graphics gc) {
        gc.setFill(1, opacity);
        //todo fillCircle
        gc.fillOval(position().x().asDouble(), position().y().asDouble(), size, size);
    }

    private void oscille() {
        if (this.velocity().x().lowerThan(-Math.random() * DEVIATION)) {
            this.acceleration().x(Math.random() / 200);
        }
        if (this.velocity().x().greaterThan(Math.random() * DEVIATION)) {
            this.acceleration().x(-Math.random() / 200);
        }
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        //
        oscille();
        //wind correction
        this.position().add(correction);
        //updating all
        super.update(screenSize, elapsedTime);
        //if offscreen bottom, put to top with new values
        if (this.position().y().greaterThan(screenSize.y())) {
            reset();
        }
        //if offscreen left, put to right
        if (this.position().x().negative()) {
            this.position().x(screenSize.x());
        }
        //if offscreen right, put to left
        if (this.position().x().greaterThan(screenSize.x())) {
            this.position().x(0);
        }
    }

    public void update(final Vect2 screenSize, final long elapsedTime, final double x, final double y) {
        this.correction.x(x);
        update(screenSize, elapsedTime);
    }

    private Vect2 randomVelocity() {
        return Vect2.of(Math.random() / 1000, (Math.random() * size + (size * size)) / 20);
    }

    private Vect2 randomStart() {
        return Vect2.of(screenSize.x().random(), screenSize.y().random().inverse());
    }

    private Vect2 randomAcceleration() {
        return Vect2.of(Math.random() / 200, Math.random() * size / 1000);
    }
}
