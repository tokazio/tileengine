package net.esyo.tileengine.engine.core;

import javafx.scene.input.MouseEvent;

/**
 *
 * @author rpetit
 */
public interface OnMouse {

    void onMouseMoved(MouseEvent e);


}
