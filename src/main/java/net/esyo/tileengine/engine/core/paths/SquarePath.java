package net.esyo.tileengine.engine.core.paths;

/**
 *
 * @author rpetit
 */
public class SquarePath implements Path {

    @Override
    public double y(double x) {
        return x * x;
    }
}
