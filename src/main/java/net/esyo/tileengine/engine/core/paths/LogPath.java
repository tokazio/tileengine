package net.esyo.tileengine.engine.core.paths;

/**
 *
 * @author rpetit
 */
public class LogPath implements Path {

    @Override
    public double y(double x) {
        return Math.log(x);
    }
}
