package net.esyo.tileengine.engine.core;

import java.util.ArrayList;
import java.util.List;
import javafx.animation.AnimationTimer;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import net.esyo.tileengine.engine.core.utils.LongValue;

/**
 * Core javaFX canvas 2D engine.
 *
 * Create a scene with a full canvas in the constructor, add key listening. Call
 * initialize method with the GraphicsContext as parameter. Create and start a
 * javaFX AnimationTimer that call the update method (at 60 fps) Show the form.
 *
 * Use:
 *
 * -Dprism.verbose=true -Dprism.order=es2,j2d -Dprism.forceGPU=true
 *
 * As run arguments to get OpenGL/DirectX hardware accelerated
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public abstract class Engine {

    private final Graphics gc;
    private final Canvas canvas;

    private final AnimationTimer timer;

    protected final List<OnKey> keyables = new ArrayList<>();
    protected final List<OnMouse> mousables = new ArrayList<>();

    public void addKeyable(final OnKey k) {
        this.keyables.add(k);
    }

    public void addMousable(final OnMouse m) {
        this.mousables.add(m);
    }

    /**
     * Create a new engine on the Stage 's', giving the width and height
     *
     * @param s Stage to use for rendering
     * @param screenSize Screen size
     */
    public Engine(final Stage s, final Vect2 screenSize) {
        //Setting the scene
        final Group root = new Group();
        final Scene theScene = new Scene(root);
        s.setScene(theScene);
        //Creating canvas on the scene
        this.canvas = new Canvas(screenSize.x().asDouble(), screenSize.y().asDouble());
        root.getChildren().add(canvas);
        //Setting keyboard events
        theScene.setOnKeyPressed(this::onKeyPress);
        theScene.setOnKeyReleased(this::onKeyUp);
        //
        theScene.setOnMouseMoved(this::onMouseMoved);
        //Getting the GraphicsContext
        this.gc = new Graphics(this.canvas.getGraphicsContext2D(), screenSize);
        //
        initialize(gc);
        //Time values
        final LongValue lastNanoTime = new LongValue(System.nanoTime());

        //Rendering loop
        this.timer = new AnimationTimer() {

            @Override
            public void handle(long currentNanoTime) {
                // calculate time since last update.
                long elapsedTime = (currentNanoTime - lastNanoTime.getValue()) / 1000000;
                //Callback update function with GraphicContext, times
                update(gc, elapsedTime);
                gc.setStroke(Color.ORANGE);
                gc.strokeText(Math.round(1000d / elapsedTime) + "FPS", 10, 20);
                //Must be last
                lastNanoTime.setValue(currentNanoTime);
            }
        };
        this.canvas.setWidth(screenSize.x().asDouble());
        this.canvas.setHeight(screenSize.y().asDouble());
        s.sizeToScene();
        //Show Stage
        s.show();
    }

    public Engine start() {
        this.timer.start();
        return this;
    }

    public Engine stop() {
        this.timer.stop();
        return this;
    }

    /**
     * Update callback which is called in each engine loop.
     *
     * @param gc
     * @param elapsedTime loop time in ms
     */
    public abstract void update(final Graphics gc, final long elapsedTime);

    /**
     * Initialize callback which is called after the GraphicsContext creation
     * and before the engine loop
     *
     * @param gc
     */
    public abstract void initialize(final Graphics gc);

    public void onKeyPress(final KeyEvent e) {
        keyables.forEach(k -> k.onKeyPress(e));
    }

    public void onKeyUp(final KeyEvent e) {
        keyables.forEach(k -> k.onKeyUp(e));
    }

    public void onMouseMoved(final MouseEvent e) {
        mousables.forEach(k -> k.onMouseMoved(e));
    }
}
