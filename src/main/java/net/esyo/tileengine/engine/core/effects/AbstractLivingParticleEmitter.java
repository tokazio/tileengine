package net.esyo.tileengine.engine.core.effects;

import java.util.ArrayList;
import java.util.List;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Positionable;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author rpetit
 * @param <P> a Particle type
 */
public abstract class AbstractLivingParticleEmitter<P extends LivingParticle> implements LivingParticleEmitter<P> {

    protected final List<P> particles = new ArrayList<>();
    protected final int number;
    protected Vect2 position;
    protected long lastCreated;

    public AbstractLivingParticleEmitter(int number) {
        this.number = number;
        if (allAtOnce()) {
            for (int i = 0; i < number; i++) {
                this.particles.add(resetParticle(createParticle()));
            }
        }
    }

    public AbstractLivingParticleEmitter(Vect2 position, int number) {
        this(number);
        this.position = position.copy();
    }

    @Override
    public <T extends Positionable> T position(Vect2 point) {
        this.position = point.copy();
        return (T) this;
    }

    private boolean allAtOnce() {
        return false;
    }

    private boolean isLooping() {
        return true;
    }

    @Override
    public P resetParticle(final P p) {
        p.rebirth().position(this.position);
        return p;
    }

    @Override
    public void render(final Graphics gc) {
        this.particles.forEach(particle -> particle.render(gc));
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        if (this.particles.size() < this.number) {
            this.particles.add(resetParticle(createParticle()));
            this.lastCreated = System.currentTimeMillis();
        }
        for (P particle : this.particles) {
            if (particle.isAlive()) {
                particle.update(screenSize, elapsedTime);
            } else if (isLooping()) {
                resetParticle(particle);
            }
        }
    }

    @Override
    public Vect2 position() {
        return this.position.copy();
    }

}
