package net.esyo.tileengine.engine.core;

/**
 *
 * @author romainpetit
 */
public interface UpdateListener {
    
    boolean beforeUpdate(long elapsedTime);
    
    void afterUpdate(long elapsedTime);
}
