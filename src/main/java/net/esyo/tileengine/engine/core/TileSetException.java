package net.esyo.tileengine.engine.core;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class TileSetException extends Exception {

    public TileSetException(String s) {
	super(s);
    }

}
