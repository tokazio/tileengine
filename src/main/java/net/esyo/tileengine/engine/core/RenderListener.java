package net.esyo.tileengine.engine.core;

/**
 *
 * @author romainpetit
 */
public interface RenderListener {
    
    boolean beforeRender(Graphics gc);
    
    void onRender(Graphics gc);

}
