package net.esyo.tileengine.engine.core;

import javafx.scene.input.KeyEvent;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public interface OnKey {

    void onKeyPress(final KeyEvent k);

    void onKeyUp(final KeyEvent k);
}
