package net.esyo.tileengine.engine.core;

/**
 *
 * @author rpetit
 */
public interface Positionable {

    <T extends Positionable> T position(final Vect2 point);

    Vect2 position();
}
