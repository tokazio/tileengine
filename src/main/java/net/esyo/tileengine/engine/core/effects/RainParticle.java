package net.esyo.tileengine.engine.core.effects;

import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author rpetit
 */
public class RainParticle extends AbstractParticle {

    private double size;
    private final Vect2 screenSize;
    private final Vect2 correction = Vect2.zero();

    public RainParticle(final Vect2 screenSize) {
        this.screenSize = screenSize;
        reset();
    }

    private void reset() {
        this.size = randomSize();
        this.position(randomStart());
        this.velocity(randomVelocity());
        this.acceleration(randomAcceleration());
    }

    private static final double GLOBAL_OPACITY = 0.8;

    @Override
    public void render(final Graphics gc) {
        //draw a faded line (maybe with an angle)
        final double decX = correction.x().mul(3).div(size).asDouble();
        double j = 0;
        for (double i = 0; i < size; i++) {
            gc.setStroke(1, (i / size) * GLOBAL_OPACITY);
            gc.strokeLine(position().x().add(j).asDouble(), position().y().add(i).asDouble(), position().x().add(j).add(decX).asDouble(), position().y().add(i).add(1).asDouble());
            j += decX;
        }
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        //map correction
        this.position().add(correction);
        //updating all
        super.update(screenSize, elapsedTime);
        //if offscreen bottom, put to top with new values
        if (this.position().y().greaterThan(this.screenSize.y())) {
            reset();
        }
        //if offscreen left, put to right
        if (this.position().x().negative()) {
            this.position().x(screenSize.x());
        }
        //if offscreen right, put to left
        if (this.position().x().greaterThan(screenSize.x())) {
            this.position().x(0);
        }
    }

    public void update(final Vect2 screenSize, final long elapsedTime, final double x, final double y) {
        this.correction.x(x);
        update(screenSize, elapsedTime);
    }

    private Vect2 randomStart() {
        return Vect2.of(screenSize.x().random(), screenSize.y().random().inverse());
    }

    private Vect2 randomVelocity() {
        return Vect2.of(0, (Math.random() * size + (size * size)) / 50);
    }

    private double randomSize() {
        return Math.random() * 15 + 2;
    }

    private Vect2 randomAcceleration() {
        return Vect2.of(0, Math.random() * size / 1000);
    }
}
