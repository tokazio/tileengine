package net.esyo.tileengine.engine.core;

/**
 *
 * @author romainpetit
 */
public interface Collidable {

    boolean intersect(final Collidable other);

    Rect2 getBounds();

    int power();

    <T extends Collidable> T hit(final Collidable c);
}
