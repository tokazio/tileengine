package net.esyo.tileengine.engine.core;

/**
 *
 * @author rpetit
 */
public enum Direction {

    NONE,
    LEFT,
    RIGHT,
    UP,
    DOWN
}
