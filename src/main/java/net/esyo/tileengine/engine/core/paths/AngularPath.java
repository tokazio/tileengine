package net.esyo.tileengine.engine.core.paths;

/**
 *
 * @author rpetit
 */
public class AngularPath implements Path {

    private final double angle;//radian

    /**
     *
     * @param angle will be bound into 90° to -90°
     */
    public AngularPath(double angle) {
        if (angle > Math.PI / 2 || angle < -(Math.PI / 2)) {
            this.angle = angle % (Math.PI / 2);
        } else {
            this.angle = angle;
        }
        System.out.println("Angle " + angle + " = " + this.angle);
    }

    @Override
    public double y(double x) {
        return Math.sin(angle) * x;
    }

}
