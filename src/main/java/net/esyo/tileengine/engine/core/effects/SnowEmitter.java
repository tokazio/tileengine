package net.esyo.tileengine.engine.core.effects;

import java.util.ArrayList;
import java.util.List;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Positionable;
import net.esyo.tileengine.engine.core.Renderable;
import net.esyo.tileengine.engine.core.Updatable;
import net.esyo.tileengine.engine.core.Vect2;

/**
 *
 * @author rpetit
 */
public class SnowEmitter implements Positionable, Renderable, Updatable {

    private final List<SnowParticle> snows = new ArrayList<>();

    public SnowEmitter(final Vect2 screenSize, final int number) {
        for (int i = 0; i < number; i++) {
            snows.add(new SnowParticle(screenSize));
        }
    }

    @Override
    public void render(final Graphics gc) {
        snows.forEach(s -> s.render(gc));
    }

    @Override
    public <T extends Positionable> T position(final Vect2 point) {
        return (T) this;
    }

    @Override
    public Vect2 position() {
        throw new UnsupportedOperationException("No position for a SnowEmitter.");
    }

    @Override
    public void update(final Vect2 screenSize, final long elapsedTime) {
        snows.forEach(s -> s.update(screenSize, elapsedTime));
    }

    public void update(final Vect2 screenSize, final long elapsedTime, final double x, final double y) {
        snows.forEach(s -> s.update(screenSize, elapsedTime, x, y));
    }
}
