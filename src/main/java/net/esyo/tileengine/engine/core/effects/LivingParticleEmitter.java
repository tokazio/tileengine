package net.esyo.tileengine.engine.core.effects;

import net.esyo.tileengine.engine.core.Positionable;
import net.esyo.tileengine.engine.core.Renderable;
import net.esyo.tileengine.engine.core.Updatable;

/**
 *
 * @author rpetit
 * @param <P> a Particle type
 */
public interface LivingParticleEmitter<P extends LivingParticle> extends Positionable, Renderable, Updatable {

    P createParticle();

    P resetParticle(final P p);
}
