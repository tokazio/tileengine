package net.esyo.tileengine.engine.core;

import net.esyo.tileengine.engine.core.utils.OnEndEvent;

/**
 * Object that can be animated (Animable)
 *
 * Animable implementation
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public abstract class AbstractAnimable implements Animable {

    protected boolean playing = false;
    protected boolean looping = false;
    public OnEndEvent end = null;

    @Override
    public boolean isPlaying() {
	return playing;
    }

    @Override
    public <T extends Animable> T play(final boolean loop) {
	looping = loop;
	playing = true;
	return (T) this;
    }

    @Override
    public <T extends Animable> T pause() {
	playing = false;
	return (T) this;
    }

    @Override
    public <T extends Animable> T resume() {
	playing = true;
	return (T) this;
    }

    @Override
    public <T extends Animable> T stop() {
	playing = false;
	return (T) this;
    }

    @Override
    public <T extends Animable> T onEnd() {
	playing = false;
	if (end != null) {
	    end.call();
	}
	return (T) this;
    }
}
