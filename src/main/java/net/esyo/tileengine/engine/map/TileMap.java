package net.esyo.tileengine.engine.map;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.paint.Color;
import net.esyo.tileengine.engine.core.Direction;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.OnRenderEvent;
import net.esyo.tileengine.engine.core.Positionable;
import net.esyo.tileengine.engine.core.Rect2;
import net.esyo.tileengine.engine.core.Renderable;
import net.esyo.tileengine.engine.core.TileSet;
import net.esyo.tileengine.engine.core.Vect2;

/**
 * Tile map rendering.
 * Handling moving screen bounding.
 * Handling collisions.
 * Handling layers.
 * OnRender is called when it's time to draw the 'externalsLayer', then you can handle z-index.
 * 1) setting externalsLayerIndex
 * 2) setting onRender callback
 * Updates can be mades in the main loop.
 *
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public abstract class TileMap implements Positionable, Renderable {

    private static final boolean DEBUG = false;
    private static final int VEL = 4;

    protected final List<Rect2> collisions = new ArrayList<>();
    protected int layersCount = 0;
    protected TileSet tiles = null;
    protected Vect2 size;
    protected Vect2 screenSize = Vect2.of(32, 32);
    protected Vect2 tileSize = Vect2.of(32, 32);

    //TODO: serialize to one D array for perf
    //Layers [num of the layer][map width][map height]
    protected int[][][] layers = null;
    protected final List<OnRenderEvent> layerRenderers = new ArrayList<>();
    protected Vect2 real = Vect2.of(0, 0);
    protected Vect2 velocity = Vect2.of(0, 0);

    public abstract <T extends TileMap> T loadFromFile(final String filename) throws Throwable;

    public boolean isMoving() {
        return this.velocity.x().notZero() || this.velocity.y().notZero();
    }

    public Vect2 real() {
        return this.real;
    }

    public Vect2 tileSize() {
        return this.tileSize;
    }

    public Vect2 screenSize() {
        return this.screenSize;
    }

    public List<Rect2> collisions() {
        return this.collisions;
    }

    public void velocity(double d) {
        this.velocity = Vect2.of(d);
    }

    public Vect2 size() {
        return this.size;
    }

    public void positionFromCenter(final Vect2 point) {
        real = Vect2.of(point.x().sub(screenSize.x().div(2)).mul(tileSize.x().inverse()), point.y().sub(screenSize.y().div(2)).mul(tileSize.y().inverse()));
    }

    public void goRight(final boolean b) {
	if (b) {
            velocity.sub(Vect2.of(VEL, 0));
	} else {
            velocity.x(0);
	}
    }

    public void goUp(final boolean b) {
	if (b) {
            velocity.add(Vect2.of(0, VEL));
	} else {
            velocity.y(0);
	}
    }

    public void goDown(final boolean b) {
	if (b) {
            velocity.sub(Vect2.of(0, VEL));
	} else {
            velocity.y(0);
	}
    }

    public void goLeft(final boolean b) {
	if (b) {
            velocity.add(Vect2.of(VEL, 0));
	} else {
            velocity.x(0);
	}
    }

    public void update(final long elapsedTime, final Rect2 rect) {
	if (isMoving()) {
	    //current position
	    double x = real.x().asDouble();
            double y = real.y().asDouble();
            //new position
            real.add(velocity);
            //can update? if not, return to current position
            collisions.stream().filter(c -> (c.intersect(Rect2.of(real.x().add(rect.position().x()).inverse(), real.y().add(rect.position().y()).inverse(), rect.size().x(), rect.size().y())))).map(_item -> {
                real.x(x);
		return _item;
            }).forEachOrdered(_item -> real.y(y));
	}
    }

    @Override
    public <T extends Positionable> T position(final Vect2 point) {
	return (T) this;
    }

    @Override
    public Vect2 position() {
        throw new RuntimeException("No position for a TileMap");
    }

    @Override
    public void render(final Graphics gc) {
	//Computing the map window
        Vect2 m = real.div(tileSize);

        //Bounds to array
        if (m.x().negative()) {
            m.x(0);
	}
        if (m.x().add(screenSize.x()).greaterThan(this.size.x())) {
            m.x(this.size.x().sub(screenSize.x()));
	}
        if (m.y().negative()) {
            m.y(0);
	}
        if (m.y().add(screenSize.y()).greaterThan(this.size.y())) {
            m.y(this.size.y().sub(screenSize.y()));
	}
	//Render
	//long s = System.nanoTime();
	for (int l = 0; l < layersCount; l++) {
            for (double y = m.y().asDouble(); y < m.y().add(this.screenSize.y()).add(1).asDouble(); y++) {
                double ty = this.tileSize.y().mul(y).asDouble();
                for (double x = m.x().asDouble(); x < m.x().add(this.screenSize.x()).add(1).asDouble(); x++) {
                    double tx = this.tileSize.x().mul(x).asDouble();
		    //Render background layer with 0's
		    if (l == 0) {
                        this.tiles.render(gc, 0, Vect2.of(real.x().add(tx), real.y().add(ty)));
		    }
		    //Render current layer
		    this.tiles.render(gc, this.layers[l][(int) x][(int) y], Vect2.of(real.x().add(tx), real.y().add(ty)));
		    //
		    try{
			renderOnLayer(l).call(gc);
		    }catch(NullPointerException ex){

		    }
		}
	    }
	}
	//Debug, draw collisions areas
	if (DEBUG) {
	    collisions.forEach((c) -> {
		gc.setStroke(Color.BLUE);
                gc.strokeRect(c.position().x().add(real.x()).asDouble(), c.position().y().add(real.y()).asDouble(), c.size().x().asDouble(), c.size().y().asDouble());
	    });
	}
    }

    public OnRenderEvent renderOnLayer(final int layerIndex) {
	return layerRenderers.get(layerIndex);
    }

    public <T extends TileMap> T layerRenderer(final int layerIndex, final OnRenderEvent onRenderEvent) {
	layerRenderers.set(layerIndex, onRenderEvent);
	return (T) this;
    }

    public Direction direction() {
        if (this.isMoving()) {
            if (velocity.x().positive()) {
                return Direction.RIGHT;
            }
            if (velocity.x().negative()) {
                return Direction.LEFT;
            }
            if (velocity.y().positive()) {
                return Direction.DOWN;
            }
            if (velocity.y().negative()) {
                return Direction.UP;
            }
        }
        return Direction.NONE;
    }

    public Vect2 velocity() {
        return this.velocity;
    }
}
