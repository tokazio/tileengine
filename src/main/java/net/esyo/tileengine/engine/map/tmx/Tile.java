package net.esyo.tileengine.engine.map.tmx;

import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Tile {

    @XmlAttribute
    private int id;

    @XmlElement
    private MapObjectGroup objectgroup;

    @Override
    public String toString() {
	return "Tile #" + id + ":\n" + objectgroup;
    }

    public int getId() {
	return id;
    }

    public List<MapObject> getObjects() {
	return objectgroup.getObjects();
    }
}
