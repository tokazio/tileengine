package net.esyo.tileengine.engine.map.tmx;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * <layer name="back" width="128" height="128">
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Layer {

    @XmlAttribute
    private String name;

    @XmlAttribute
    private int width;

    @XmlAttribute
    private int height;

    @XmlElement
    private Data data;

    public String getName() {
	return name;
    }

    public int getWidth() {
	return width;
    }

    public int getHeight() {
	return height;
    }

    public Data getData() {
	return data;
    }

    @Override
    public String toString() {
	return this.getName() + " " + this.getWidth() + "," + this.getHeight();
    }
}
