package net.esyo.tileengine.engine.map.tmx;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;

/**
 * <tileset firstgid="1" name="tilea1" tilewidth="32" tileheight="32" tilecount="1504" columns="8">
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class TileSet {

    @XmlAttribute
    private int firstgid;

    @XmlAttribute
    private String name;

    @XmlAttribute
    private int tilewidth;

    @XmlAttribute
    private int tileheight;

    @XmlAttribute
    private int tilecount;

    @XmlAttribute
    private int columns;

    @XmlElement
    private Image image;

    @XmlElements({
       	@XmlElement(name = "tile", type = Tile.class),})
    private final List<Tile> tiles = new ArrayList<>();

    public int getFirstgid() {
	return firstgid;
    }

    public String getName() {
	return name;
    }

    public int getTilewidth() {
	return tilewidth;
    }

    public int getTileheight() {
	return tileheight;
    }

    public int getTilecount() {
	return tilecount;
    }

    public int getColumns() {
	return columns;
    }

    public Image getImage() {
	return image;
    }

    public List<Tile> getTiles() {
	return tiles;
    }

    public Tile getTileById(final int tileId) throws MapException {
	for(Tile t:tiles){
	    if(t.getId()==tileId){
		return t;
	    }
	}
	throw new MapException("No tile object with id '"+tileId+"'");
    }

    @Override
    public String toString(){
	StringBuilder s = new StringBuilder();
	s.append(this.name).append(" from ").append(this.image).append(" (").append(this.tilecount).append(" tiles)").append("\n");
	s.append("Tiles:\n");
	for(Tile t:tiles){
	    s.append(t).append("\n");
	}
	return s.toString();
    }
}
