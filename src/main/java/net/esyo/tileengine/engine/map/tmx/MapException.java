package net.esyo.tileengine.engine.map.tmx;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class MapException extends Exception {

    public MapException(String s) {
	super(s);
    }

}
