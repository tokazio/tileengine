package net.esyo.tileengine.engine.map.tmx;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * <image source="tilea1.png" width="256" height="6016"/>
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Image {

    @XmlAttribute
    private String source;

    @XmlAttribute
    private int width;

    @XmlAttribute
    private int height;

    public String getSource() {
	return source;
    }

    public int getWidth() {
	return width;
    }

    public int getHeight() {
	return height;
    }

    @Override
    public String toString() {
	return source + " (" + width + "," + height + ")";
    }
}
