package net.esyo.tileengine.engine.map.tmx;

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public final class Loader {

    private Loader(){
	//Hide constructor
    }

    public static Map load(String filename) throws JAXBException {
        final JAXBContext jaxbContext = JAXBContext.newInstance(Map.class);
        final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	return (Map) jaxbUnmarshaller.unmarshal(new File(filename));
    }
}
