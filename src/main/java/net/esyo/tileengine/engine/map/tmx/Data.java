package net.esyo.tileengine.engine.map.tmx;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * <data encoding="csv">
 * 
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Data {
    
    @XmlAttribute
    private String encoding;
    
    @XmlValue
    private String dataStr;
    
    public String[] csv() throws MapException{
	if(!"csv".equals(encoding)){
	    throw new MapException("No CSV datas");
	}
	return dataStr.split(",");
    }
}
