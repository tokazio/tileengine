package net.esyo.tileengine.engine.map.tmx;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
class MapObjectGroup {

    @XmlAttribute
    private String draworder;

    @XmlAttribute
    private String color;

    @XmlAttribute
    private String name;

    @XmlElements({
       	@XmlElement(name = "object", type = MapObject.class),})
    private final List<MapObject> objects = new ArrayList<>();

    public int countObjects() {
	return this.objects.size();
    }

    public MapObject get(final int index) {
	return (index>0 && index<countObjects()) ? objects.get(index) : null;
    }

    public String getDraworder() {
	return draworder;
    }

    public String getColor() {
	return color;
    }

    public String getName() {
	return name;
    }

    public List<MapObject> getObjects() {
	return objects;
    }

    @Override
    public String toString(){
	StringBuilder s = new StringBuilder();
	s.append("Object group '").append(name).append("':\n");
	for(MapObject o:objects){
	    s.append(o).append("\n");
	}
	return s.toString();
    }
}
