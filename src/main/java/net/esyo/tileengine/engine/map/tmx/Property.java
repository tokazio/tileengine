package net.esyo.tileengine.engine.map.tmx;

import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * <property name="screenheight" type="int" value="16"/>
 * <property name="screenwidth" type="int" value="16"/>
 *
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Property{

    @XmlAttribute
    private String name;

    @XmlAttribute
    private String value;

    public String getName(){
	return this.name;
    }

    public String getValue(){
	return this.value;
    }

    @Override
    public String toString(){
	return getName()+"="+getValue();
    }
}
