package net.esyo.tileengine.demo;

import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javax.xml.bind.JAXBException;
import net.esyo.tileengine.engine.core.BezierCubicCurve;
import net.esyo.tileengine.engine.core.Engine;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.Renderable;
import net.esyo.tileengine.engine.core.TileSetException;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.map.tmx.MapException;

/**
 *
 * @author rpetit
 */
public class BezierDemo extends Application {

    private static final Vect2 SCREEN = Vect2.of(320, 240);
    private static final double SEGMENT_COUNT = 15;

    @Override
    public void start(Stage theStage) throws FileNotFoundException, IOException, TileSetException, JAXBException, MapException {

        final Bezier curveLinear = new Bezier(Vect2.of(0, 0), Vect2.of(1, 1));
        final Bezier curveOutSin = new Bezier(Vect2.of(0, 0), Vect2.of(0.25, 1));
        final Bezier curveInOut = new Bezier(Vect2.of(0.75, 0), Vect2.of(0.25, 1));
        final Bezier curveInSin = new Bezier(Vect2.of(0.75, 0), Vect2.of(1, 1));

        //2D engine to render the tiles/sprites
        new Engine(theStage, SCREEN) {

            @Override
            public void initialize(Graphics gc) {
                //do nothing
            }

            @Override
            public void update(Graphics gc, long elapsedTime) {
                gc.setFill(1);
                gc.fillRect(SCREEN);
                //
                gc.setLineWidth(1);
                //
                gc.setStroke(Color.GREEN);
                curveLinear.render(gc);
                gc.setStroke(Color.ORANGE);
                curveOutSin.render(gc);
                gc.setStroke(Color.RED);
                curveInOut.render(gc);
                gc.setStroke(Color.VIOLET);
                curveInSin.render(gc);
            }
        }.start();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private static class Bezier implements Renderable {

        private final BezierCubicCurve curve;

        public Bezier(final Vect2 p1, final Vect2 p2) {
            this.curve = new BezierCubicCurve(p1, p2);
        }

        @Override
        public void render(Graphics gc) {
            Vect2 q0 = curve.point(0);
            for (int i = 1; i <= SEGMENT_COUNT; i++) {
                Vect2 q1 = curve.point(i / SEGMENT_COUNT).mul(SCREEN);
                gc.strokeLine(q0, q1);
                q0 = q1;
            }
        }
    }
}
