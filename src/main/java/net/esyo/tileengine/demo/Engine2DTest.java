package net.esyo.tileengine.demo;

import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.application.Application;
import javafx.geometry.Point2D;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javax.xml.bind.JAXBException;
import net.esyo.tileengine.engine.core.AnimatedSprite;
import net.esyo.tileengine.engine.core.Engine;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.KeyFrames;
import net.esyo.tileengine.engine.core.Sprite;
import net.esyo.tileengine.engine.core.TileSet;
import net.esyo.tileengine.engine.core.TileSetException;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.text.Dialog;
import net.esyo.tileengine.engine.core.text.LetterByLetterText;
import net.esyo.tileengine.engine.map.tmx.MapException;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public class Engine2DTest extends Application {

    private static final int DURATION = 128;

   // private Dialog dia;

    @Override
    public void start(Stage theStage) throws FileNotFoundException, IOException, TileSetException, JAXBException, MapException {
	//Loading multiple tile sets
//	TileSet t = new TileSet("src/main/resources/Riou.png", Vect2.of(32, 48)).transparent(Vect2.zero());
		TileSet t = new TileSet("src/main/resources/toutnu.png", Vect2.of(96, 145));//.transparent(Vect2.zero());

//        TileSet tb = new TileSet("src/main/resources/Riou-Battle.png", Vect2.of(32, 64)).transparent(Vect2.zero());
	//New fixed sprite using a tile set
	Sprite sprite = new Sprite(t, 0, Vect2.zero()).position(50,50);
	//New animated sprite using the tile sets
	AnimatedSprite d = new AnimatedSprite(t, Vect2.of(0, 0)).addKeyFrames("DOWN", new KeyFrames(2, 1,0,1,2,3,4,3)).duration(DURATION).play(true);
/*        AnimatedSprite u = new AnimatedSprite(t, Vect2.of(48, 0)).addKeyFrames("UP", new KeyFrames(13, 12, 13, 14)).duration(DURATION).play(true);
        AnimatedSprite l = new AnimatedSprite(t, Vect2.of(96, 0)).addKeyFrames("LEFT", new KeyFrames(25, 24, 25, 26)).duration(DURATION).play(true);
        AnimatedSprite r = new AnimatedSprite(t, Vect2.of(128, 0)).addKeyFrames("RIGHT", new KeyFrames(37, 36, 37, 38)).duration(DURATION).play(true);

        AnimatedSprite dr = new AnimatedSprite(t, Vect2.of(0, 48)).addKeyFrames("DOWN_RUN", new KeyFrames(3, 4, 5, 6, 7, 8)).duration(DURATION).play(true);
        AnimatedSprite ur = new AnimatedSprite(t, Vect2.of(48, 48)).addKeyFrames("UP_RUN", new KeyFrames(15, 16, 17, 18, 19, 20)).duration(DURATION).play(true);
        AnimatedSprite lr = new AnimatedSprite(t, Vect2.of(96, 48)).addKeyFrames("LEFT_RUN", new KeyFrames(27, 28, 29, 30, 31, 32)).duration(DURATION).play(true);
        AnimatedSprite rr = new AnimatedSprite(t, Vect2.of(128, 48)).addKeyFrames("RIGHT_RUN", new KeyFrames(39, 40, 41, 42, 43, 44)).duration(DURATION).play(true);

        AnimatedSprite rc = new AnimatedSprite(t, Vect2.of(150, 48)).addKeyFrames("RUN_CLOUD", new KeyFrames(9, 10, 11, 21, 22, 23)).duration(DURATION).play(true);

        AnimatedSprite b = new AnimatedSprite(tb, Vect2.of(0, 96)).addKeyFrames("BATTLE", new KeyFrames(0, 1, 2, 3)).duration(DURATION).play(true);
*/
        //2D engine to render the tiles/sprites
	new Engine(theStage, Vect2.of(32 * 16, 32 * 16)) {

	    @Override
            public void initialize(final Graphics gc) {
		//gc.setFont(Font.font("Menlo",12));
	//	LetterByLetterText.getTextSize(gc);
		//
	//	dia = new Dialog(new Point2D(10,10),"Test de la boîte de dialogue. Il faut une ligne super grande pour voir comment elle sera coupée et il faut aussi tester le saut de ligne!\n A partir d'ici j'ai sauté une ligne\n deux lignes \n et même trois!!").play(false);
//		this.addKeyable(dia);
	    }

	    @Override
            public void update(final Graphics gc, final long elapsedTime) {
		gc.clearRect(0, 0, 32 * 16, 32 * 16);

        //        dia.update(gc.screenSize(), elapsedTime);

//		sprite.render(gc);

		d.render(gc);
/*
		r.render(gc);
		l.render(gc);
		u.render(gc);

		dr.render(gc);
		rr.render(gc);
		lr.render(gc);
		ur.render(gc);

		rc.render(gc);

		b.render(gc);

                dia.render(gc);
*/
            }
	}.start();
    }

    public static void main(String[] args) {
	launch(args);
    }

}
