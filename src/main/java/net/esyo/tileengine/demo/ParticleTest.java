package net.esyo.tileengine.demo;

import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javax.xml.bind.JAXBException;
import net.esyo.tileengine.engine.core.Engine;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.TileSetException;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.effects.CircleParticleEmitter;
import net.esyo.tileengine.engine.core.effects.LivingParticleEmitter;
import net.esyo.tileengine.engine.map.tmx.MapException;

/**
 *
 * @author rpetit
 */
public class ParticleTest extends Application {

    private final Vect2 screenSize = Vect2.of(32 * 16, 32 * 16);
    private final int number = 100;

    private double x = 0;
    private double vx = 1;
    private final double y = screenSize.y().half().asDouble();

    @Override
    public void start(Stage theStage) throws FileNotFoundException, IOException, TileSetException, JAXBException, MapException {

        final LivingParticleEmitter emitter = new CircleParticleEmitter(number);

        //2D engine to render the tiles/sprites
        new Engine(theStage, screenSize) {

            @Override
            public void initialize(Graphics gc) {

            }

            @Override
            public void update(Graphics gc, long elapsedTime) {
                gc.setFill(Color.BLACK);
                gc.fillRect(screenSize);
                emitter.position(Vect2.of(x, y));
                x += vx;
                if (x < 0 || x > screenSize.x().asDouble()) {
                    vx = -x;
                }
                emitter.update(screenSize, elapsedTime);
                emitter.render(gc);
            }
        }.start();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
