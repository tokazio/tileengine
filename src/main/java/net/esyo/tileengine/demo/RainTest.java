package net.esyo.tileengine.demo;

import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javax.xml.bind.JAXBException;
import net.esyo.tileengine.engine.core.Engine;
import net.esyo.tileengine.engine.core.Graphics;
import net.esyo.tileengine.engine.core.OnMouse;
import net.esyo.tileengine.engine.core.TileSetException;
import net.esyo.tileengine.engine.core.Vect2;
import net.esyo.tileengine.engine.core.effects.RainEmitter;
import net.esyo.tileengine.engine.map.tmx.MapException;

/**
 *
 * @author rpetit
 */
public class RainTest extends Application implements OnMouse {

    private final Vect2 screenSize = Vect2.of(32 * 16, 32 * 16);
    private final int number = 1000;

    @Override
    public void start(Stage theStage) throws FileNotFoundException, IOException, TileSetException, JAXBException, MapException {
        final RainEmitter rain = new RainEmitter(screenSize, number);

        //2D engine to render the tiles/sprites
        new Engine(theStage, screenSize) {

            @Override
            public void initialize(Graphics gc) {
                this.addMousable(RainTest.this);
            }

            @Override
            public void update(Graphics gc, long elapsedTime) {
                gc.setFill(112, 171, 70);
                gc.fillRect(gc.screenSize());
                rain.update(gc.screenSize(), elapsedTime, mx, 0);
                rain.render(gc);
            }
        }.start();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private double mx = 0;

    @Override
    public void onMouseMoved(MouseEvent e) {
        mx = (e.getSceneX() / screenSize.x().sub(0.5).asDouble()) / 2;
    }
}
