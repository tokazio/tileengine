package net.esyo.tileengine.engine.core.paths;

import java.util.Arrays;
import java.util.Collection;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author rpetit
 */
@RunWith(Parameterized.class)
public class AngularPathTest {

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{{15, 10, 2.588}, {90, 5, 5}, {15, 5, 1.294}, {-15, 5, -1.294}, {105, 10, 2.588}, {-105, 10, -2.588}});
    }

    private final double angle;//deg
    private final double verifY;
    private final double x;

    public AngularPathTest(final double angle, final double x, final double verifY) {
        this.angle = angle;
        this.x = x;
        this.verifY = verifY;
    }

    @Test
    public void test() {
        //given

        //when
        final double y = new AngularPath(Math.toRadians(angle)).y(x);

        //then
        assertThat(y).isCloseTo(verifY, within(0.001));
    }

}
