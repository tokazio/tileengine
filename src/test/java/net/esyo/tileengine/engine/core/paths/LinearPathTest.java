package net.esyo.tileengine.engine.core.paths;

import java.util.Arrays;
import java.util.Collection;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author rpetit
 */
@RunWith(Parameterized.class)
public class LinearPathTest {

    @Parameters
    public static Collection<Object> data() {
        return Arrays.asList(new Object[]{5, 7, 985, 6, 854, 42, 41, 53, 22, 852});
    }

    private final double x;

    public LinearPathTest(final double x) {
        this.x = x;
    }

    @Test
    public void test() {
        //given

        //when
        final double y = new LinearPath(10).y(x);

        //then
        assertThat(y).isEqualTo(10);
    }

}
