package net.esyo.tileengine.engine.core;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import org.junit.Test;

/**
 *
 * @author rpetit
 */
public class Vect2Test {

    @Test
    public void testAngle() {
        //given
        final Vect2 a = Vect2.of(3, 4);
        final Vect2 b = Vect2.of(4, 3);

        //when
        final double angle = a.angle(b).asDouble();

        //then
        assertThat(angle).isCloseTo(16.260, within(0.001));
    }

    @Test
    public void testAngle2() {
        //given
        final Vect2 a = Vect2.of(7, 1);
        final Vect2 b = Vect2.of(5, 5);

        //when
        final double angle = a.angle(b).asDouble();

        //then
        assertThat(angle).isCloseTo(36.869, within(0.001));
    }

    @Test
    public void testSub() {
        //given
        final Vect2 a = Vect2.of(7, 1);
        final Vect2 b = Vect2.of(5, 5);

        //when
        final Vect2 v = b.sub(a);

        //then
        assertThat(v.x().asDouble()).isEqualTo(-2);
        assertThat(v.y().asDouble()).isEqualTo(4);
    }

    @Test
    public void testAngleHz() {
        //given
        final Vect2 b = Vect2.of(5, 5);

        //when
        final double v = b.angle();

        //then
        assertThat(v).isEqualTo(Math.PI / 4);
    }
}
